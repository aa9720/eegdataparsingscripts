# EEG Processing

[toc]

## About

This is the MSEE thesis work of Andrew Allard from RIT (Rochester Institute of Technology).  The idea is to train an AI system to predict seizures using the [datasets](##data-sets-used) below then look at the AI architecture to see what the AI learned.  From this information attempt to find a feature, group of features, or pattern for predicting seizures.  If these features are not unique to each individual a non-AI system should be able to be created from this system.  If each individual has a unique set of features that are different for each person this system should also show that.

## Data sets used

* [CHB-MIT](https://physionet.org/content/chbmit/1.0.0/)
  * One of the best known EEG datasets though it is quite old.
* [TUH EEG Corpus](https://www.isip.piconepress.com/projects/tuh_eeg/html/downloads.shtml)
  * A HUGE dataset that is still being updated.
  * Not just for seizure prediction but for other EEG features as well.

## Architecture

The architecture of the NN was based loosely off of [this paper](https://ieeexplore.ieee.org/abstract/document/8765420).  The actual architecture used was

![](latex/figures/nnFormat.png)
