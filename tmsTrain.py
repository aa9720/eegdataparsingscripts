#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 12:12:32 2021

@author: allard
"""

import argparse
import os
from tmsAugment import findWeightsFile
import numpy as np

def main(eps:int=500,
         batchSize:int=16,
         fnCmts:str='',
         ratio:float=0.5,
         edfStart:int=None,
         numEdfsToUse:int=None,
         patient:int=1,
         doubleData:bool=False,
         quadrupleData:bool=False,
         textAlert:bool=False,
         uofr:bool=False,
         evalAllWeights:bool=False):
    from aiCHBMITclass import nerualNet, neuralNetUR
    import platform
    from txtAlert import sendText
    if not uofr:
        nnC = nerualNet(patient=patient)
        nnC.edfStartingFile = 10
        nnC.numEDFsToUse = 7
    else:
        nnC = neuralNetUR(patient=patient)

    if edfStart is not None:
        nnC.edfStartingFile = edfStart
    if numEdfsToUse is not None:
        nnC.numEDFsToUse = numEdfsToUse
    nnC.loadCHBdata()

    nnC.showDataStats()

    nn = nnC.buildNN()
    if isinstance(ratio, float):
        nnC.dropExcessData(ratio)
    elif isinstance(ratio, str):
        if not ratio.lower() =='all':
            nnC.dropExcessData(float(ratio))

    nnC.showDataStats()

    ls = nnC.trainModel(epochs=eps,
                        batch_size=batchSize,
                        fnComments=fnCmts,
                        doubleDataSize=doubleData,
                        quadDataSize=quadrupleData)
    if evalAllWeights:
        def ev(f):
            try:
                nnC.model.load_weights(findWeightsFile(f))
            except:
                return 0,0
            ret =  nnC.evalModel(full=True, calcAUC=True)
            print('File: {0}\tROC: {1:.3f}'.format(f, ret[0]))
            return ret
        aucs = [ev(f)[0] for f in ls[1]]
        aucs = np.array(aucs)
        bestNet = ls[1][aucs.argmax()]
        ls = ls, bestNet
    res = nnC.evalModel(full=True)
    print(res)
    if textAlert:
        sendText('Train done patient {2}: {0:.4f}, {1:.4f}'.format(res[0][0], res[1][0], patient))
    return ls


if __name__ == '__main__':

    psr = argparse.ArgumentParser(description='This script starts the training for the CHB-MIT AI used to learn to predict patient specific seizures')

    psr.add_argument('patient', type=int, help='The patient to train the AI to predict seizures for')
    psr.add_argument('--uofr', type=bool, help='whether or not to use the U of R data for training.', required=False, default=False)
    psr.add_argument('--edf_start', help='EDF file to start with', default=0, type=int, required=False)
    psr.add_argument('--num_edfs', help='Number of EDF files to use', default=7, type=int, required=False)

    psr.add_argument('--bs', help='Batch size to be used.', default=16, type=int, required=False)
    psr.add_argument('--eps', help='Number of Epochs to train the AI with.', default=500, type=int, required=False)
    psr.add_argument('--fc', help='Comments to put on the end of the file name', default='', type=str, required=False)
    psr.add_argument('--ratio', help='Ratio of preictal to non preictial', default=0.5, required=False)
    psr.add_argument('--doubleData', help='Whether or not to double the data array by flipping it around the time axis.', dest='doubleData', action='store_true',default=False)
    psr.add_argument('--quadData', help='Whether or not to qaudruple the data array by flipping it around the time axis, the channel axis and both (this will override doubleData).', dest='quadData', action='store_true',default=False)
    psr.add_argument('--txt', required=False, default=False, action='store_true', dest='txtAlert', help='If added a text alert will be sent after training is done.')

    args = psr.parse_args()
    main(eps=args.eps,
         batchSize=args.bs,
         fnCmts=args.fc,
         ratio=args.ratio,
         edfStart=args.edf_start,
         numEdfsToUse=args.num_edfs,
         patient=args.patient,
         doubleData=args.doubleData,
         quadrupleData=args.quadData,
         textAlert=args.txtAlert,
         uofr=args.uofr)
