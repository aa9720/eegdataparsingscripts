# Allard note log
### 9/3/2021
* Reworked all the AI into a nice single file for better readability and debugging.
* Learned today the normalization equation in the paper I have been using is actually another layer in the AI not a regularization equation applied to the raw data.
* Found the added dropout in the desktop version of the AI seems to be performing ~0.92 rather than the cuda at ~0.78.
	* Not entirely sure.  Also this is only in the training phase have not evaluated any of these to see if this is actually doing anything useful.
* CudnnLSTM has twice the biases of CPU (generic) LSTM from keras.
	* This might explain some of why the cuda box seems to be slower at gathering a pattern.
	* Even so the biases are taken care of when loading GPU trained weights into CPU LSTM.


### 9/4/2021
* Finding values of up to ~24 inside the NN.
	* This seems strange to me.
	* Quick search turns up this is normal for relu type activations.  The output of the NN is sigmoid though which means it is inherently bound between 0 and 1.
* For this initial sucessful training of the NN (done on the desktop)
	* 500 epochs
	* first 7 edf files
	* batch size 16
	* Results:
		* loss: 0.01786 accuracy: 0.99484 (last improvement on epoch 487.  Which means I could likely have gotten a bit better if I let it run another 100 or so epochs)
* Output of LSTM (input to fully connected layer) for several ictal states:
	* ![Pasted image 20210904134922](assets/Pasted%20image%2020210904134922.png)
	* Weights of output layer:
		* ![Pasted image 20210904135117](assets/Pasted%20image%2020210904135117.png)
	* Based on the weights certain LSTM cells learn what the preictal state is while others learn what the precital state is not.  This then gets weighted such that the real preictal cells are weighted high while the non-preictal cells are weighted negatively high.
	* Furthermore it seems the preictal state is relatively close in structure compared to the non-ictal state. (These conclusions are based on one training session on one patient so they may not hold across either _must keep that in mind_)
	* Model summary:
		* ![Pasted image 20210904142317](assets/Pasted%20image%2020210904142317.png)
* Cuda computer seems to never be working.  Going to try dropping in a regular LSTM rather than the CuDNN version.
	* One notable difference is the version of tensorflow (Cuda box=2.3.0 automation=2.6.0)
	* Going to start automation on patient 3 and move cuda box to a regular LSTM.  (will be much slower)
* Moving the cuda box to the regular LSTM does indeed make it much more accurate.  But it comes at a time cost.  an 8x time cost....  Still faster than the 
* Trying to add some dropout to the cudnnlstm and see if that works.
	* This seems to be working..... Will check back later
	* Initially it made some headway then seemed to bottom out.
		* I did change 2 things on that run (I upped the number of edfs and added dropout.  Now trying with fewer edfs and dropout.)
	* It seems 16 edfs is good. 32 is bad.  Have no idea why...
		* Over fitting maybe due to the amount of non seizure data?
* automation is not finding anything good with patient 3 ATM.  I'll let it run the 500 epochs to see if it pulls something out of if I need to hyperparamter tune.
* cudnnLSTM even with dropout does not seem to do as well as the pure cpu learning running on automation.
	* I may switch to  partial cpu partial gpu on cuda 5.
	* cuda 4 is running doing pure cpu training.  If it proves to have similar performance to automation I will likely start training on it for simplicities sake.

* Found quite a bug!  I forgot to set the file offset.  This is why none of the systems would figure out any patient beyond 1.
	* There was more to that bug than I thought.  Now I think it is solved
	* Also I found a bug that shouldn't have bitten me but may have before I was done.

### 9/5/2021
* Cuda box did quite well on patient 1 with the added dropout.
	* Moved cuda box on to patient 3
* I restarted automation with the updated settings (as expected it did quite poorly with the bad settings)
	* It didn't seem to be doing well at first.  I added a 7th file and it seemed to pick up some.
* If the cuda box continues to do as well as it seems to be ATM I'll likely be using that for training and automation/Allard-PC for evaluation purposes.

### 9/6/2021
* Can we model an AI as an LTI? (_update 9/13: not likely_)
	* Doing so would allow me to plot pole-zero plots for the NNs for each patient which would allow for easy comparison.

### 9/13/2021
* Over the weekend I tried to train the same NN on patient 1 and patient 3 data.
	* The NN did terrible even after 5000 epochs.
		* 0.5525 loss.
		* 0.7592 accuracy.
	* As previously thought it is not likely to be able to predict preictal activity for more than one patient.
* Had a good talk with Dr Berg today see [9 13 2021](noteFiles/questionsForDrBerg.md#9%2013%202021) for details.
* **What would happen if we were to combine the [MAX78000](https://www.maximintegrated.com/en/products/microcontrollers/MAX78000.html) with an EEG ADC and make a new [RNS](https://www.neuropace.com)**

### 9/17/2021
* Made a visualizer for the LSTM hidden state and output state yesterday.
	* This is handy but still doesn't make the AI more understandble.
* Interesting finding.... __All of this is for patient #1 from the CHB-MIT dataset__
	* Below are plots of max, min, mean, and std of the features (first two dimensions of the 4D array) of the last convolution layer:
		![](assets/Pasted%20image%2020210917143112.png)
		![](assets/Pasted%20image%2020210917143120.png)
		![](assets/Pasted%20image%2020210917143127.png)
		![](assets/Pasted%20image%2020210917143137.png)
	* For the above images the x axis is feature and the y axis is each feature.
	* Note the lines of low valued features in max, min, and std images.  Interestingly enough this does not seem to hold at the output of the layer.
	* This is the output of the same layer, first for non preictal then for preictal:
		![](assets/Pasted%20image%2020210917143507.png)
		![](assets/Pasted%20image%2020210917143518.png)
	* This would be the input to the LSTM.  (x axis is features y axis is time ).
	* Note how the preictal plot has a substantially higher output compared to the non preical.  This is quite strange when compared to the activations of the LSTM over time.  In the images again, first is non preictal followed by preictal
		![](assets/Pasted%20image%2020210917143756.png)
		![](assets/Pasted%20image%2020210917143807.png)
	* Thus the LSTM activation is much higher for a non preictal eeg epoch.
	* The weights for the Dense layer (output layer directly after the LSTM) (plotted above) average out to 0.0171 with an std of 0.9269.  Odd considering the activations of the LSTM.
* Horizontal lines (in the feature plots above) indicate a feature from the previous layer that is not weighted heavily vertical lines indicate features that have a very low weight and will be very small when passed to the next layer.
* Allard is dumb......  LSTMs operate on a single instance of the time series data at a time.  The output of the layer is merely the output of the LSTMs at the end of evaluating all the timeseries data.  LSTMs don't actually operate on the entire data set they get.  Instead they iterate through it and then dump the last value out.
	* This is likely why LSTMs are so slow on the CPUs as they take stupid amounts of time to run through all the calculations needed.

### 9/20/2021
* Widening the features (from 2x3 to 2x5 or 2x7) gives the NN more weights to tune.
	* At the cost of training time the NN does seem to perform better but it requires more training epochs.
* Attempting to train a new NN with a reduced number of features to see if it can gain a decent accuracy or if the 32 feature set is important.
### 9/22/2021
* Training a crazy reduced network (non bidirectional LSTM and CNN with only 32,16,16,8 feature sets rather than the 32 for all) still trains quite well.
	* Have not evaluated on non training data.
* Next step is going to be making eeg data.
	* Start with some funny waveforms and modify it until it triggers the network.  How do I tweak it though???

### 9/23/2021
* Potential system bug.
	* I found the very good patient 1 predictions from the NN trained on automation might be partly from the fact there is only one section of preictal.
	* Going to reduce the number of non-preictal points in hopes of getting a better trained net.  And faster.

### 9/24/2021
* Put in a drop to reduce the amount of non preictal data the NN is trained on.
* NN weight save files will now have the host name in them to insure I know where they came from.
* Maybe making a GAN next to attempt to generate preictal data.
	* This could help figure out what the NN is triggering on.

### 9/25/2021
* MNE has different PSD methods depending on the type of data input.
	* For raw files they use the welch method for calculating PSD.
	* For epochs they use multitaper which is supposed to be better as getting quality information at lower frequencies.
* Training an NN with no LSTM to see if this is more of a feature problem or more of a time series of feature problem (seizure prediction)
* _Ideas for future_:
	* Plot multitaper PSD with each channel on a plot.
	* Plot the output of the final CNN layer as a PSD to see how it compares to the PSD
		* Maybe able to quick transform from one to the other?

### 9/27/2021
* Started the cuda box training a regular LSTM rather than the much faster CuDNNLSTM.
	* I think something strange is happening with the CuDNNLSTM causing it to be less accurate but substantially faster than the standard LSTM.
* Turns out putting a fully connected layer directly after a flattened output from the last CNN is slightly more accurate than an LSTM (99.88% rather than 99.165%).
	* The FC converged at epoch 278 the LSTM version had its last improvement at epoch 484 so it likely would have gotten better within the next 10-25 epochs.
	* _Should run a 800 epoch over night here to see if it does improve more_
* It appears for patient 1 the power spectra indicate when the power at frequencies above ~12Hz drops down a seizure seems to be coming on.
	* (best look at this figure in a new tab as this is a huge image)  In the figure below the plots on the left is non preictal data and the plots on the right are preictal data.  Each subplot is a separate channel.  Each series is a separate epoch (5s).  The orange series on both figures are identical.  It is a sp![](assets/Pasted%20image%2020210927143232.png)ectrum that is non preictal but was marked as preictal by the NN (standard normal NN no mods.)
![](assets/Pasted%20image%2020210927143232.png)

### 9/28/2021
* New idea....
	* Use Deconvolution (conv2Dtranspose) to figure out how each feature back propagated through the net looks.
	* This is going to be a long tedious process that Allard will need to automate to make it make sense.

### 9/29/2021
* Something seems to be up today.  Non of the NNs want to train correctly.
	* Maybe I am just not patient enough....
	* I think updating ipython to 7.28.0 caused some type of issue.
		* Not sure what it is or why.  But it was after that nothing seemed to optimize correctly.
		* From now on Allard will note any package installs or upgrades here.

### 9/30/2021
* Installing keras-tuner on GLE computer.
* Had a meeting with Dr Berg and Steve Erickson.
	* Meeting went well.  Had an overview of seizure types and how they relate to EEG.

### 10/1/2021
* Cranky!  It's October already!
	* Allard you have 3 months to wrap this dissertation up.  Better get moving!
* Cuda box did incredibly well on patient 3 training to a loss of 0.00559 in under 2k epochs.
	* This is odd as it usually does terrible on patient 1.
	* Will retry patient 1 later to see if this was solved in some update or if this is just because cuda is good at patient 3 and not 1....
	* Verified cuda does not train well on patient 1.  But it is doing very good on patient 7.

### 10/4/2021
* Update to the nnviewer.  I realized today the sbps were not on the same Z (color) scale.  Fixed this so I'm not comparing apples and bananas

### 10/5/2021
* augmented Grad-cam python stuff: https://github.com/diegocarrera89/AugmentedGradCAM
* Standard grad cam keras: https://keras.io/examples/vision/grad_cam/
* _File was removed as it is now tracked in the dissertation_


### 10/8/2021
* Added a procedure/proposal to the repo so Dr Berg can see about getting data for me. [thesisProcedure](thesisProcedure.md)
* Worked on dissertation getting the methodology framed correctly.

### 10/13/2021
* On automation I updated pip and installed ann_vizualizer and graphviz

### 10/14/2021
* Thing to track for each augmentation of the data when AGCAMing:
	* Rotation
	* Translation
	* Wrapped
	* Rolled
	* Roll amount

### 10/17/2021
* Batch normalization is applied between every CNN layers
	* This was Initially missed during the NN design
	* Fixed now.
* AG-CAM git repo mentioned in [10 5 2021](allardNoteLog.md#10%205%202021) was designed for TF 1.
	* Updating in progress to get to TF 2 level.
	* Have the augmenter mostly done.
	* Need to update the superresolution portion next then test.
* Testing of AG-CAM scripts will take place after re-training of the NN now that batch normalization is applied between layers.


### 10/25/2021
* Started looking at alternate datasets.
	* see [dataSets](noteFiles/dataSets.md) for a list and details

### 10/27/2021
* Removing the input batch normalization layer makes the training less likely to converge as well.  Thus harming the overall performance.
	* Batch normalization removes any dc offset from the data.
	* Batch normalization insures the input is between 0 and 1.
	* BN 
## Spring semester
### 1/18/2022
* Started a new NN training with the following settings:
	* Patient: 1
	* BS: 64
	* SF: 7
	* Num Files: 22
	* Outputs: 1
	* binary crossentripy loss
	* ratio: 0.4
* Starting another iteration with the following settings:
	* Patient: 3
	* BS: 64
	* SF: 3
	* Num Files: 22
	* Outputs: 1
	* binary crossentripy loss
	* ratio: 0.4
### 1/19/2022
 * Starting another iteration with the following settings:
	* Patient: 7
	* BS: 64
	* SF: 0
	* Num Files: 14
	* Outputs: 1
	* binary crossentripy loss
	* ratio: 0.4
* Starting another iteration with the following settings:
	* Patient: 10
	* BS: 64
	* SF: 18
	* Num Files: all
	* Outputs: 1
	* binary crossentripy loss
	* ratio: 0.4

Began augmenting:
* Patient 1:
	* Augs: 25
	* start file: 0
	* num Files: 6
	* file used ep500
### 1/23/2022
Having many issues with overfitting.  Trying to add dropout into the net to see if that helps at all.
Attempts on patients 1,3,7 train well but are so badly overfit the predictions of new data are worthless.
### 1/24/2022
Overfitting is still dogging me.
* Added dropout on to the entire net.
* Now reducing CNN features to 16.
	* If that doesn't work will try adding more dropout.
	* If that still doesn't remove the overfitting issue I will have to develop plan B.....
### 1/25/2022
* I theorize there is something substantially different between the different seizures for each patient in the CHB-MIT database.
	* Doing some analysis on patient 1 it appears the variance of the channels is notably different from the first two seizures recorded to the last two.
	* This could be due to the hours of day the recordings were taken as well.
### 1/26/2022
* Have data from Dr Berg/Steve.
	* Only 1-4 files per patient.
	* Need to write new parser to take into account this new data.
* Parser thinking:
	* Load **ALL** files for a given patient.
	* Drop randomly the non-preictal data to make ~50% of the data preictal.
		* Record the indexes of these dropped points
	* Take 80% of the remaining data for training and use the leftover for validation.
		* To do this I will need to record the indexes of data used for training
### 02/02/2022
* UofR data data notes
	* From Dr Berg and Steve:
		* 10 names of people who had surgery to put implants on the brain for focal epilepsy.
		* Two different recording policies.
			* The first looked for the first change (clinical change or behavior or electrical onset) which indicated the start of a seizure.  From 2 minutes before the seizure until the seizure was recorded everything else was scrapped.
			* The second kept all data rather than snippets.
	* There seem to be 4.33333 patients that can be realtively easily used.
		* Patients 1 and 6 seem like a very short bit of coding and could be training.
		* Patients 2, 8 and 10a seem like they could work quite well.
	* ==It seems like the snippets policy ended at the U of R at the end of 2018.==
	* Patients that seem very likely to have snippets:
		* 3, 4, 5a, 5b, 7


### 2/15/2022
* Allard found a HUGE error.  The ai script when loading the data in was placing seizures at the wrong place.  This means all of the various icals data were in the wrong location.  After fixing this things started to make sense as to which features and or channels are most prominent in pre-ictal labeling.
### 2/16/2022 (24)
* After retraining on all 4 patients and testing the trainings as well as reaugmenting for patient 1 and 3 all that is left to do is test training and reaugment for 7 and 21.  Once this is done Allard will analyze the data and report.  (In theory this should only take a couple weeks.)
	* This is good as Allard only has 6w and 2d to get a final draft of the dissertation done.
### 3/7/2022
* Been working on Dr Berg data.  Starting a new notes file here [URdataNotes](noteFiles/URdataNotes.md).
* 