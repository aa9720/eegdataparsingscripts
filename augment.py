#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 17:08:19 2021

@author: allard

The scripts in this file are loosely based off of
https://github.com/diegocarrera89/AugmentedGradCAM/blob/master/lib_aug_gradcam.py

But due to different versions of tensorflow and incompatability with what
this project the two classes below have been almostly completely rewritten.

"""
import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa



class Augmenter:
    # adapted from: https://github.com/diegocarrera89/AugmentedGradCAM/blob/master/lib_aug_gradcam.py
    # This has been heavily modified from the original
    def __init__(self, num_aug, nn, cnnLayer, augcamsz=(1, 156), imgsz=(23, 1280)):
        self.num_aug = num_aug
        self.imgsz = np.array(imgsz)
        self.augcamsz = augcamsz
        self.initVars()
        self.nn = nn
        self.cnnLayer = cnnLayer
        self.grad_model = tf.keras.models.Model(
            [self.nn.inputs], [self.nn.get_layer(self.cnnLayer).output, self.nn.output]
        )

    def initVars(self):
        # Reduced the augmentation range SEPT 28 2022.
        randInitA = tf.random_uniform_initializer(-0.5, 0.5)
        augSize = np.round(np.array(self.imgsz) / 4)
        augSize = (int(augSize[0]), int(augSize[1]))
        print(augSize)
        randInitS = tf.random_uniform_initializer([-augSize[1], -augSize[0]], [augSize[1], augSize[0]])
        self.tns_angle = tf.Variable(randInitA([self.num_aug]))
        self.tns_shift = tf.Variable(randInitS([self.num_aug, 2]))
        # self.tns_input_img = tf.Variable(randInit([self.imgsz[0], self.imgsz[1], 1]))
        self.tns_img_batch = tf.Variable(randInitA([self.num_aug, self.augcamsz[0], self.augcamsz[1], 1]))

    @tf.function
    def direct_augment(self, img, angles=None, shifts=None):
        """ Apply rotation and shift to `img` according to the values in `angles` and `shift`.
        :param img: the input image
        :param angles: the magnitude of the rotation in radians
        :param shift: the magnitude of the shift
        :return img_aug: the transformed image
        """
        if angles is not None:
            self.tns_angle.assign(angles)
        if shifts is not None:
            self.tns_shift.assign(shifts)

        tns_img_exp = tf.tile(tf.expand_dims(img, 0), [self.num_aug, 1, 1, 1])
        # Changed fill_mode to wrap NOV 29 2022
        # This will mean EEG is wrapped around rather than simply cut off
        # during angular augmentation.
        tns_rot_img = tfa.image.rotate(tns_img_exp, self.tns_angle, interpolation="BILINEAR", fill_mode='wrap')
        return tfa.image.translate(tns_rot_img, self.tns_shift, interpolation="BILINEAR", fill_mode='wrap')

    @tf.function
    def inverse_augment(self, img_batch, angles, shift):
        """ Apply the inverse rotation and shift to `img_batch` according to the values in `angles` and `shift`.
        :param img_batch: a set of images to be anti-transformed
        :param angles: the magnitude of the rotatation in radians
        :param shift: the magnitude of the shift
        :return img_aug: the anti-transformed image
        """
        feed_dict = {self.tns_img_batch: img_batch,
                     self.tns_angle: -np.array(angles),
                     self.tns_shift: -np.array(shift),
                     }
        img_aug = self.tns_input_aug(feed_dict)

        return img_aug

    @tf.function
    def getAugs(self, img, angs=None, shifts=None, nn=None, cnnLayer=None):
        if nn is not None:
            self.nn = nn
        if cnnLayer is not None:
            self.cnnLayer = cnnLayer
        if any([self.cnnLayer is None, self.nn is None]):
            raise Exception('Need a model and a layer please set!!!')

        return self.make_gradcam_heatmap(
            self.direct_augment(img, angs, shifts),
            nn,
            cnnLayer)


    @tf.function
    def make_gradcam_heatmap(self, img, nn, cnnLayer=None, pred_index=None):
        # First, we create a model that maps the input image to the activations
        # of the last conv layer as well as the output predictions
        if nn is not None:
            self.nn = nn
        if cnnLayer is not None:
            self.cnnLayer = cnnLayer
        if any([self.cnnLayer is None, self.nn is None]):
            raise Exception('Need a model and a layer please set!!!')


        # Then, we compute the gradient of the top predicted class for our input image
        # with respect to the activations of the last conv layer
        with tf.GradientTape() as tape:
            layer_output, preds = self.grad_model(img)
            # if pred_index is None:
            #     pred_index = tf.argmax(preds[0])
            class_channel = preds[0]

        # This is the gradient of the output neuron (top predicted or chosen)
        # with regard to the output feature map of the last conv layer
        # This is a vector where each entry is the mean intensity of the gradient
        # over a specific feature map channel
        pooled_grads = tf.reduce_mean(
            tape.gradient(class_channel,
                          layer_output),
            axis=(0, 1, 2))

        # We multiply each channel in the feature map array
        # by "how important this channel is" with regard to the top predicted class
        # then sum all the channels to obtain the heatmap class activation
        # layer_output = layer_output[0]
        heatmap = tf.matmul(layer_output,
                            pooled_grads[...,
                                         tf.newaxis])
        # heatmap = tf.squeeze(heatmap, -1)

        # For visualization purpose, we will also normalize the heatmap between 0 & 1
        heatmap = tf.square(heatmap / tf.math.reduce_max(tf.abs(heatmap)))
        return heatmap


class Superresolution:
    # Loosely based around: https://github.com/diegocarrera89/AugmentedGradCAM/blob/master/lib_aug_gradcam.py#L414
    # Due to different versions of tensorflow and a few other odities this class has had a
    # Complete rewrite.
    def __init__(self, augmenter, learning_rate=0.1):
        num_aug = augmenter.num_aug
        self.augmenter = augmenter
        randInitA = tf.random_uniform_initializer(0, 1)
        self.fullCam = tf.Variable(randInitA((*self.augmenter.imgsz, 1)), trainable=True)
        self.cams = tf.Variable(tf.zeros((self.augmenter.num_aug, *self.augmenter.augcamsz, 1)))
        self.optimizer = tf.optimizers.Adam(learning_rate)
        self.lambda_eng = tf.Variable([1.0])
        self.lambda_tv = tf.Variable([1.0])
        self.functionaMixed = tf.Variable(42.0)
        # self.augCamszself.augmenter.augcamsz
        #self.optimizer.minimize(self.lossFunc)

    @tf.function
    def lossFunc(self):
        camDS = tf.image.resize(self.augmenter.direct_augment(self.fullCam),
                              (self.augmenter.augcamsz))
        camGrads = tf.image.image_gradients(tf.expand_dims(self.fullCam, -1))

        tns_df = tf.reduce_sum(tf.square(tf.subtract(camDS, self.cams)))
        tns_tv = tf.reduce_sum(tf.add(tf.abs(camGrads[0]), tf.abs(camGrads[1])))
        tns_norm = tf.reduce_sum(tf.square(self.fullCam))

        functional_tv = tf.add(tns_df, tf.scalar_mul(self.lambda_tv[0], tns_tv))
        return tf.add(tf.scalar_mul(self.lambda_eng[0], tns_norm), functional_tv)

    @tf.function
    def runCam(self, cams=None):
        if cams is not None:
            self.cams.assign(cams)
        with tf.GradientTape() as tp:
            tp.watch(self.fullCam)
            l = self.lossFunc()
            g = tp.gradient(l, [self.fullCam])
        self.optimizer.apply_gradients(zip(g, [self.fullCam]))
        return l, g

    @tf.function
    def runmanyCams(self, numRuns=25):
        for i in range(numRuns):
            l,g = self.runCam()

    @tf.function
    def runCamSection(self, dtaArray, nn, cnnLayer):
        self.cams.assign(self.augmenter.getAugs(dtaArray, nn=nn, cnnLayer=cnnLayer))
        self.optimizer.learning_rate.assign(0.01)
        self.runmanyCams(250)
        self.optimizer.learning_rate.assign(0.001)
        self.runmanyCams(200)
        self.optimizer.learning_rate.assign(0.0001)
        self.runmanyCams(100)
