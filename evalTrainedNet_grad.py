#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 20 08:24:48 2022

@author: allard
"""

import matplotlib.pyplot as p
import numpy as np
import pickle
import bz2
from visTools import plotEEGwithCAM
from aiCHBMITclass import nerualNet
from tmsAugment import findWeightsFile
from sklearn.metrics import roc_curve, auc
import plotly.express as px
import plotly.graph_objs as go
from plotly.subplots import make_subplots


#%% Initial setup


patient = 1
saveImg = False
loadAugs = True
openFig = True
# 1=320  3=243  7=480  21 = 468
edfsToUse = {1:[0,6], 3:[30,6], 7:[11,2], 21:[16,6]}
# 7 original 10, 6.  Using 11, 2 as it has plenty of data.
netEP = {1:'320', 3:'243', 7:'480', 21:'468'}

edfStart = edfsToUse[patient][0]
numEdfsToUse = edfsToUse[patient][1]
nnC = nerualNet(patient=patient)
nnC.edfStartingFile = edfStart
nnC.numEDFsToUse = numEdfsToUse
nnC.loadData(False)
nn = nnC.buildNN()
print(nn.predict(nnC.xSlices[3:4])) # Force the building of the net


#%% Load Augs
wsFileRe = '.+CHB.+patient{0}-.+ep{1}.+'.format(patient, netEP[patient])

if loadAugs:
    with bz2.BZ2File('augData/gradCamsPatient{0}.pbz2'.format(patient),'rb') as f:
        grads = pickle.load(f)
        grads = [np.array(g).squeeze(axis=(1,-1)) for g in grads]

if patient == 7 and numEdfsToUse == 2:
    grads = grads[2881:2881+nnC.ySlices.shape[0]]

#%% Testing weights



#nnC.dropExcessData(ratio)

nn.load_weights(findWeightsFile(wsFileRe))


preds = nn.predict(nnC.xSlices, verbose=0)


#%%
fig = make_subplots(rows=1, cols=2, subplot_titles=('NN predictions and actual preictal probability','Receiver operating characteristic'))

idx=0
t1 = dict(y=preds[:,idx], name='NN output', mode='markers', marker_size=4, marker_opacity=0.4, marker_color='blue')
# t2 = dict(y=nnC.ySlices[:,idx], name='Expected Output', mode='markers', marker_size=4, marker_opacity=0.1, marker_color='red')
fig.add_traces(data=[t1], rows=1, cols=1)


def makeRects(fig, array, sbpIdx=(1,1), label='Seizure', color='green', opacity=0.5):
    toPlot= array.copy()
    indexes = np.where(toPlot==1)[0]

    diffed = np.diff(indexes)
    ictalStarts = indexes[[True, *(diffed>1)]]
    ictalEnds = indexes[[*(diffed>1), True]]

    for icS, icE in zip(ictalStarts, ictalEnds):
        fig.add_shape(x0=icS,
                      x1=icE,
                      y0=0,
                      y1=1,
                      name='Ictal',
                      type='rect',
                      fillcolor=color,
                      opacity=opacity,
                      line_color='rgba(0,128,0,0)',
                      row=sbpIdx[0],
                      col=sbpIdx[1])
        fig.add_annotation(
            text=label,
            x=(icE+icS)/2,
            y=1,
            ay=-40,
            ax=-0.1,
            arrowhead=1,
            showarrow=True,
            textangle=-45,
            row=sbpIdx[0],
            col=sbpIdx[1])

makeRects(fig, nnC.ySlices[:,1])
makeRects(fig, nnC.ySlices[:,0], label='Preictal', color='orange', opacity=0.3)

for fEnd in np.cumsum(nnC.fileLengths) / nnC.patientFileInfo['sfreq'] / 5:
    fig.add_annotation(
        text='EOF',
        x=fEnd,
        y=0,
        ay=65,
        ax=0.1,
        arrowhead=1,
        showarrow=True,
        textangle=-45,
        row=1,
        col=1)


ax = fig.get_subplot(1,1)

ax[0]['title']='EEG epochs'
ax[1]['title']='Proability of EEG epoch being preictal'
ax[1]['range']=[-0.01,1.1]

#%

fpr, tpr, _ = roc_curve(nnC.ySlices[:,0],preds)
area = auc(fpr, tpr)
t1 = dict(x=fpr, y=tpr, name='ROC (area=%0.2f)' % area, fill='tozeroy', fillcolor='rgba(0,0,255,0.3)', line_color='blue')


print(area)
t2 = dict(x=[0, 1], y=[0, 1], line_color="navy", line_dash="dash", name='Linear')

fig2 = fig.add_traces(data=[t1,t2], rows=1, cols=2)


ax2 = fig.get_subplot(1,2)
ax2[1]['title']='True Positive Rate'
ax2[0]['title']='False Positive Rate'
ax2[0]['range']=[0,1]
ax2[1]['range']=[0,1.05]

fig.update_layout(title='<b>Fit of NN for patient {0}</b>'.format(patient))

if openFig:
    fig.show('browser')
if saveImg:
    with open('latex/figures/pat{0}.jpeg'.format(patient),'wb') as fi:
        fi.write(fig.to_image('jpeg', width=1500, height=750))


#%% Plot grads

idx = 2334

fg = make_subplots(2,
                   2,
                   subplot_titles=[
                       'Grad Cam of the first layer',
                       'Grad Cam of the second layer',
                       'Grad Cam of the third layer',
                       'Grad Cam of the fourth layer'],
                   specs=([[{}, {'t':0.05,'b':0.05}],
                           [{'t':0.1,'b':0.1}, {'t':0.15,'b':0.15}]]))

fg.add_trace(go.Heatmap(z=(grads[0][idx] - grads[0][idx].min()) / (grads[0][idx].max() - grads[0][idx].min()),
                        colorscale='Jet',
                        zmax=1,
                        zmin=0), row=1, col=1)
fg.add_trace(go.Heatmap(z=grads[1][idx],
                        colorscale='Jet',
                        zmax=1,
                        zmin=0), row=1, col=2)
fg.add_trace(go.Heatmap(z=grads[2][idx],
                        colorscale='Jet',
                        zmax=1,
                        zmin=0), row=2, col=1)
fg.add_trace(go.Heatmap(z=grads[3][idx],
                        colorscale='Jet',
                        zmax=1,
                        zmin=0), row=2, col=2)

states = ['non-preictal', 'preictal']
tl = 'Pat: {3} EEG Ep: {0}<br>Pred: <i>{1}</i> Act: <i>{2}</i></b>'.format(
    idx,
    states[1 if preds[idx]>0.8 else 0],
    states[1 if nnC.ySlices[idx, 0]==1 else 0],
    patient)

fg.update_layout(title='<b>Grad-CAMs from neural net {0}'.format(tl))

fg.show('browser')


mkMov = False
#%%
if mkMov:
    'this'
    #%%
    camIdx = 2
    initialIdx = 1900
    initalHM = go.Heatmap(z=grads[camIdx][initialIdx],
                            colorscale='Jet',
                            zmax=1,
                            zmin=0)
    frames = []
    for i in range(800):
        hm = go.Heatmap(z=grads[camIdx][i+initialIdx],
                                colorscale='Jet',
                                zmax=1,
                                zmin=0)
        # create the button
        button = {
            "type": "buttons",
            "buttons": [
                {"label": "Play",
                    "method": "animate",
                    "args": [None, {"frame": {"duration": 200}, 'fromcurrent':True}],},
                {"label": "Pause",
                    "method": "animate",
                    "args": [None, {'frame':{'duration':0, 'redraw':False}, 'mode':'immediate', 'transition':{'duration':0}}]}
            ],
        }
        lay = go.Layout(updatemenus=[button],
                        title_text='EEG frame: %g'%i)
        frames.append(go.Frame(data=[hm],
                               layout=lay))
    fig = go.Figure(data=[initalHM],
                    frames=frames,
                    layout=lay)
    fig.show('browser')
