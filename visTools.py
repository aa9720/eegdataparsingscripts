#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 15 13:50:48 2021

@author: allard

A collection of various functions for plotting various things related to the
NN, its weights, and its eeg data.
"""

import matplotlib.pyplot as p
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Bidirectional, LSTM
from tensorflow.keras.layers import Convolution2DTranspose
from nerualNetViewer import get_activations
import numpy as np
import plotly.express as px
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import pandas as pd


def plotLSTMWeights(ws, bidrectional=True, transpose=True):
    preTitle = ['Forward', 'Reverse']
    titles = ['Input Weights','Recurrent Weights']
    sbpTitles = ['Forget', 'Input', 'Cell', 'Output']
    for bd in range(bidrectional+1):
        for w in range(2):
            f, sbps = p.subplots(4,1)
            f.set_label(titles[w])
            f.set_tight_layout(True)
            for i in range(4):
                units = int(ws[w+3*bd].shape[1] / 4)
                if transpose:
                    sbps[i].plot(ws[w+3*bd][:,i*units:(i+1)*units].T)
                else:
                    sbps[i].plot(ws[w+3*bd][:,i*units:(i+1)*units])
                sbps[i].grid(True)
                sbps[i].set_title(preTitle[bd] + ' ' + titles[w] + ' ' + sbpTitles[i])


def getLSTM_states(weightsLSTM, dataInput):
    bd = Bidirectional(LSTM(20, dropout=0.5, activation='sigmoid', return_sequences=True, return_state=True))
    bd(dataInput)
    bd.set_weights(weightsLSTM)
    output = [a.numpy() for a in bd(dataInput)]
    return output


def plotLSTM(ws, dataIn):
    output = getLSTM_states(ws, dataIn)
    p.figure()
    p.pcolormesh(output[0].squeeze().T, cmap='jet')
    p.colorbar()
    p.title('Activations over time')
    p.figure()
    p.subplot(2,1,1)
    p.plot(output[1].squeeze())
    p.plot(output[3].squeeze())
    p.legend(['Forward', 'Reverse'])
    p.title('Output of LSTM')
    p.grid()
    p.subplot(2,1,2)
    p.plot(output[2].squeeze())
    p.plot(output[4].squeeze())
    p.legend(['Forward', 'Reverse'])
    p.title('Hidden state of LSTM')
    p.grid()


def plotCNNWeights(ws, axes=(0,1)):
    f = p.figure();
    f.set_tight_layout(True)
    p.subplot(2,2,1);p.pcolormesh(ws.max(axis=axes), cmap='jet');p.colorbar()
    p.title('Max of the features')
    p.subplot(2,2,2);p.pcolormesh(ws.min(axis=axes), cmap='jet');p.colorbar()
    p.title('Min of the features')
    p.subplot(2,2,3);p.pcolormesh(ws.mean(axis=axes), cmap='jet');p.colorbar()
    p.title('Mean of the features')
    p.subplot(2,2,4);p.pcolormesh(ws.std(axis=axes), cmap='jet');p.colorbar()
    p.title('std of the features')


def buildDeConv():
    # This is only the beginning.....
    dec = Sequential()
    dec.add(Convolution2DTranspose(1, input_shape=(22, 1278, 32), strides=(1,1), kernel_size=(2,3)))
    dec.compile('rmsprop',
                'binary_crossentropy',
                metrics=['accuracy'])
    return dec


def plotEEG_slice(eegSlice, chNames=None, maxT=5, ax=None):
    xAr = np.linspace(0, maxT, eegSlice.shape[1])
    yAr = np.arange(0, eegSlice.shape[0])
    eegSlice = eegSlice.squeeze()[::-1]
    dt = eegSlice - eegSlice.mean(0)
    dt = (dt.T / (np.abs(dt).max()/0.5).reshape(1, -1)).T
    dt += (np.tile(yAr.reshape((eegSlice.shape[0],1)), (1,eegSlice.shape[1])))
    if ax is None:
        f = p.figure(figsize=(8,6), tight_layout=True)
        sc = p.plot(np.tile(xAr.reshape(eegSlice.shape[1],1), (1,eegSlice.shape[0])),
                    dt.T,
                    lw=0.5, c='k',
                    alpha=1)
        ax = f.get_axes()[0]
    else:
        ax.plot(np.tile(xAr.reshape(eegSlice.shape[1],1), (1,eegSlice.shape[0])),
                dt.T,
                lw=0.5, c='k',
                alpha=1)
        f = ax.get_figure()
    ax.set_xlim(0, maxT)
    ax.grid(True)
    if chNames is not None:
        ax.set_yticks(np.arange(0,eegSlice.shape[0]),labels=chNames[::-1])
        ax.minorticks_off()

    return f


def plotEEGwithCAM(nnC,
                   augs,
                   sliceIndex,
                   normAugs=False,
                   includeMaxMin=True,
                   normEachChan=True,
                   preds=None,
                   useMatplotlib=False):
    if isinstance(sliceIndex, list):
        fg = [plotEEGwithCAM(nnC, augs, idx, preds=preds, useMatplotlib=useMatplotlib, normAugs=normAugs, normEachChan=normEachChan, includeMaxMin=includeMaxMin) for idx in sliceIndex]
        return fg
    if preds is None:
        pred = nnC.model.predict(nnC.xSlices[sliceIndex:sliceIndex+1]).squeeze()
    else:
        pred = preds[sliceIndex].squeeze()
    fc = augs[sliceIndex].copy()
    fcMin = fc.min()
    fcMax = fc.max()
    fc -= fc.min()
    if normAugs:
        fc /= fc.max()


    yAr = np.arange(0, nnC.patientFileInfo['nchan'])
    xAr = np.linspace(0, nnC.sliceLength, nnC.sliceLengthSamples)

    dt = nnC.xSlices[sliceIndex:sliceIndex+1].squeeze().copy().T
    dt -= dt.mean(0)
    if normEachChan:
        dt /= (np.abs(dt).max(0)/0.5)
    else:
        dt /= (np.abs(dt).max()/0.5)
    dt += (np.tile(yAr.reshape((nnC.patientFileInfo['nchan'],1)), (1,nnC.sliceLengthSamples)).T)

    yArM = np.arange(0, 24)-0.5
    xArM = np.linspace(0, nnC.sliceLength, nnC.sliceLengthSamples+1)#-0.5/nnC.sliceLengthSamples*nnC.sliceLength
    X,Y = np.meshgrid(xArM, yArM)


    states = ['non-preictal', 'preictal']
    if useMatplotlib:
        f = p.figure(figsize=(8,6))
        sc = p.plot(np.tile(xAr.reshape(nnC.sliceLengthSamples,1), (1,23)),
                    dt,
                    lw=0.5,
                    c='k')
        f.set_tight_layout(True)
        p.title('EEG Ep: {0} Pred: {1} Act: {2}'.format(
            sliceIndex,
            states[1 if pred>0.8 else 0],
            states[1 if nnC.ySlices[sliceIndex, 0]==1 else 0]),
            weight='bold')
        p.xlabel('Time [s]', weight='bold')
        p.yticks(np.arange(0,nnC.patientFileInfo['nchan']),
                 labels=nnC.patientFileInfo.ch_names)
        f.get_axes()[0].minorticks_off()
        out = f.get_axes()[0].pcolormesh(X,
                                         Y,
                                         fc,
                                         cmap='jet',
                                         alpha=0.7,
                                         vmin=0,
                                         vmax=1)
        p.colorbar(out)
        p.xlim(0, nnC.sliceLength)

    df = pd.DataFrame(dt,
                      columns=nnC.patientFileInfo.ch_names)
    df['Time'] = xAr
    sp = [go.Scatter(y=df[df.columns[i]],
                     x=df['Time'],
                     line_color='black',
                     line_width=0.6,
                     name=df.columns[i]) for i in range(df.shape[1]-1)]
    hm = go.Heatmap(z=fc,
                    x=xArM,
                    opacity=0.7,
                    colorscale='Jet',
                    zmax=1,
                    zmin=0)
    if includeMaxMin:
        tl = '<b>Pat: {3} EEG Ep: {0}<br>Pred: <i>{1}</i> Act: <i>{2}</i></b><br>Min: {4:.3} Max: {5:.3}'.format(
            sliceIndex,
            states[1 if pred>0.8 else 0],
            states[1 if nnC.ySlices[sliceIndex, 0]==1 else 0],
            nnC.currentPatient,
            fcMin,
            fcMax)
    else:
        tl = '<b>Pat: {3} EEG Ep: {0}<br>Pred: <i>{1}</i> Act: <i>{2}</i></b>'.format(
            sliceIndex,
            states[1 if pred>0.8 else 0],
            states[1 if nnC.ySlices[sliceIndex, 0]==1 else 0],
            nnC.currentPatient)
    fig = go.Figure(data=[hm,*sp])
    fig.update_layout(xaxis_range=(0, nnC.sliceLength),
                      title=tl,
                       xaxis_title='Time[s]',
                       yaxis_title='Channels',
                       yaxis_tickvals=np.arange(0,nnC.patientFileInfo['nchan']),
                       yaxis_ticktext=nnC.patientFileInfo.ch_names,
                       showlegend=False)
    fig.show('browser')
    return fig
