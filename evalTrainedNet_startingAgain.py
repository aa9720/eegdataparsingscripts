#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 20 08:24:48 2022

@author: allard
"""

import matplotlib.pyplot as p
import numpy as np
import pickle
import bz2
from visTools import plotEEGwithCAM
from aiCHBMITclass import nerualNet, neuralNetUR
from tmsAugment import findWeightsFile
from sklearn.metrics import roc_curve, auc
import plotly.express as px
import plotly.graph_objs as go
from plotly.subplots import make_subplots


#%% Initial setup


patient = 3
saveImg = False
loadAugs = True
openFig = True

UR = False
if not UR:
    # 1=320  3=243  7=480  21 = 468
    edfsToUse = {1:[0,6], 3:[30,6], 7:[11,2], 21:[16,6]}
    # 7 original 10, 6.  Using 11, 2 as it has plenty of data.
    netEP = {1:'320', 3:'357', 7:'480', 21:'468'}
    preFix = 'CHB'
    imgPreFix = ''
else:
    edfsToUse = {1: [2,1], 2: [0,1], 6: [2,1]}
    netEP = {1: '496', 2: '496', 6: '498'}
    preFix = 'UR'
    imgPreFix = 'UR'


edfStart = edfsToUse[patient][0]
numEdfsToUse = edfsToUse[patient][1]
if not UR:
    nnC = nerualNet(patient=patient)
else:
    nnC = neuralNetUR(patient=patient)
nnC.edfStartingFile = edfStart
nnC.numEDFsToUse = numEdfsToUse
nnC.loadData(False)
nn = nnC.buildNN()
print(nn.predict(nnC.xSlices[3:4])) # Force the building of the net


#%% Load Augs
wsFileRe = '.+{2}.+patient{0}-.+ep{1}.+'.format(patient, netEP[patient], preFix)

augFile = 'augData/{0}patient{1}FullStartingOver2.pbz2'.format(preFix, patient)

if loadAugs:
    with bz2.BZ2File(augFile,'rb') as f:
        augs = np.array(pickle.load(f))

if patient == 7 and numEdfsToUse == 2:
    augs = augs[2881:2881+nnC.ySlices.shape[0]]

#%% Testing weights



#nnC.dropExcessData(ratio)

nn.load_weights(findWeightsFile(wsFileRe))


preds = nn.predict(nnC.xSlices, verbose=0)


#%%
fig = make_subplots(rows=1, cols=2, subplot_titles=('NN predictions Vs Actual Classifictaion','Receiver operating characteristic'))

idx=0
t1 = dict(y=preds[:,idx], name='NN output', mode='markers', marker_size=4, marker_opacity=0.4, marker_color='blue')
# t2 = dict(y=nnC.ySlices[:,idx], name='Expected Output', mode='markers', marker_size=4, marker_opacity=0.1, marker_color='red')
fig.add_traces(data=[t1], rows=1, cols=1)


def makeRects(fig, array, sbpIdx=(1,1), label='Seizure', color='green', opacity=0.5):
    toPlot= array.copy()
    indexes = np.where(toPlot==1)[0]

    diffed = np.diff(indexes)
    ictalStarts = indexes[[True, *(diffed>1)]]
    ictalEnds = indexes[[*(diffed>1), True]]

    for icS, icE in zip(ictalStarts, ictalEnds):
        fig.add_shape(x0=icS,
                      x1=icE,
                      y0=0,
                      y1=1,
                      name='Ictal',
                      type='rect',
                      fillcolor=color,
                      opacity=opacity,
                      line_color='rgba(0,128,0,0)',
                      row=sbpIdx[0],
                      col=sbpIdx[1])
        fig.add_annotation(
            text=label,
            x=(icE+icS)/2,
            y=1,
            ay=-40,
            ax=-0.1,
            arrowhead=1,
            showarrow=True,
            textangle=-45,
            row=sbpIdx[0],
            col=sbpIdx[1])

makeRects(fig, nnC.ySlices[:,1])
makeRects(fig, nnC.ySlices[:,0], label='Preictal', color='orange', opacity=0.3)

for fEnd in np.cumsum(nnC.fileLengths) / nnC.patientFileInfo['sfreq'] / nnC.sliceLength:
    fig.add_annotation(
        text='EOF',
        x=fEnd,
        y=0,
        ay=65,
        ax=0.1,
        arrowhead=1,
        showarrow=True,
        textangle=-45,
        row=1,
        col=1)


ax = fig.get_subplot(1,1)

ax[0]['title']='EEG epochs'
ax[1]['title']='NN certainty of EEG epoch being preictal'
ax[1]['range']=[-0.01,1.1]

#%

fpr, tpr, _ = roc_curve(nnC.ySlices[:,0],preds)
area = auc(fpr, tpr)
t1 = dict(x=fpr, y=tpr, name='ROC (area=%0.2f)' % area, fill='tozeroy', fillcolor='rgba(0,0,255,0.3)', line_color='blue')


print(area)
t2 = dict(x=[0, 1], y=[0, 1], line_color="navy", line_dash="dash", name='Linear')

fig2 = fig.add_traces(data=[t1,t2], rows=1, cols=2)


ax2 = fig.get_subplot(1,2)
ax2[1]['title']='True Positive Rate'
ax2[0]['title']='False Positive Rate'
ax2[0]['range']=[0,1]
ax2[1]['range']=[0,1.05]

fig.update_layout(title='<b>Fit of NN for patient {0}</b>'.format(patient))

if openFig:
    fig.show('browser')
if saveImg:
    with open('thesis/figures/{1}pat{0}.jpeg'.format(patient,imgPreFix),'wb') as fi:
        fi.write(fig.to_image('jpeg', width=1500, height=750))


#%% Plot aug maxes

channelMaxes = augs.max(2).argmax(axis=1)


fig = go.Figure()
fig.add_traces(data=[go.Histogram(x=channelMaxes[nnC.ySlices[:,0]==1],
                                  name='Preictal',
                                  opacity=1,
                                  histnorm='probability'),
                     go.Histogram(x=channelMaxes[nnC.ySlices[:,0]==0],
                                  name='non-preictal',
                                  opacity=1,
                                  histnorm='probability')])
fig.update_layout(title='<b>Probability of being the highest activation channel<br>within an EEG epoch Patient %g</b>' % patient,
                  xaxis_title='EEG channels',
                  yaxis_title='Probability of being the highest activation channel',
                  xaxis_tickvals=np.arange(0,nnC.patientFileInfo['nchan']),
                  xaxis_ticktext=nnC.patientFileInfo.ch_names,
                  yaxis_range=(0,1))
if openFig:
    fig.show('browser')
if saveImg:
    with open('thesis/figures/{1}pat{0}_augChannelProb.jpeg'.format(patient, imgPreFix),'wb') as fi:
        fi.write(fig.to_image('jpeg', width=1000, height=800))

timeMaxes = augs.max(1).argmax(axis=1)


fig = go.Figure()
fig.add_traces(data=[go.Histogram(x=timeMaxes[nnC.ySlices[:,0]==1],
                                  name='Preictal',
                                  opacity=1,
                                  histnorm='probability',
                                  xbins=dict(
                                      start=0,
                                      end=nnC.sliceLengthSamples,
                                      size=40)),
                     go.Histogram(x=timeMaxes[nnC.ySlices[:,0]==0],
                                  name='non-preictal',
                                  opacity=1,
                                  histnorm='probability',
                                  xbins=dict(
                                      start=0,
                                      end=nnC.sliceLengthSamples,
                                      size=40))])
fig.update_layout(title='<b>Probability of being the highest activation time<br>within an EEG epoch Patient %g (40 sample bin width)</b>' % (patient),
                  xaxis_title='EEG epoch sample [samples]',
                  yaxis_title='Probability of being the highest activation Time',
                  xaxis_range=[0, nnC.sliceLengthSamples*1.05],
                  yaxis_range=(0,1))
if openFig:
    fig.show('browser')
if saveImg:
    with open('thesis/figures/{1}pat{0}_augTimeProb.jpeg'.format(patient, imgPreFix),'wb') as fi:
        fi.write(fig.to_image('jpeg', width=1000, height=800))
