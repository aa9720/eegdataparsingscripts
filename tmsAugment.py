#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  4 15:18:20 2021

@author: allard
"""

import argparse
import pickle
from alive_progress import alive_bar
import re
from tools import getFilesByEXT
from txtAlert import sendText
import bz2

def findWeightsFile(regex, supressMultiError=False):
    if 'savedWeights' in regex and 'h5' in regex:
        return regex
    try:
        fileList = getFilesByEXT('./savedWeights','h5')
    except FileNotFoundError:
        fileList = []
    fileList.extend(getFilesByEXT('./bestNets','h5'))
    file = [f for f in fileList if re.match(regex, f)]
    if len(file) > 1 and not supressMultiError:
        print(len(file))
        raise Exception('Need more specific regex')
    if len(file) == 1:
        print('Using File: {0}'.format(file[0]))
    return file[0]


def main(netToUse:str='',
         patient:int=1,
         edfStart:int=0,
         numEdfsToUse:int=3,
         ratio:float=0.5,
         numAugs:int=100,
         sliceList:list=[3],
         uofr:bool=False):
    from augment import Augmenter, Superresolution
    from aiCHBMITclass import nerualNet, neuralNetUR
    if not uofr:
        nnC = nerualNet(patient=patient)
    else:
        nnC = neuralNetUR(patient=patient)
    nnC.edfStartingFile = edfStart
    nnC.numEDFsToUse = numEdfsToUse
    nnC.loadData(False)
    nn = nnC.buildNN()
    print(nn.predict(nnC.xSlices[3:4])) # Force the building of the net
    if isinstance(ratio, float):
        nnC.dropExcessData(ratio)
    if isinstance(sliceList, str):
        if sliceList.lower() == 'all':
            sliceList = range(nnC.xSlices.shape[0])
        else:
            sliceList = [int(a) for a in sliceList.split(',')]
    elif isinstance(sliceList, int):
        sliceList = [sliceList]
    if not netToUse == '' and not netToUse is None:
        nn.load_weights(findWeightsFile(netToUse))
    else:
        nn.load_weights('bestNets/weightsTF234_moreData-patient1-kgcoe-cuda-05-ep499-ls0.00244-ac0.99986_MoreData_singleBatch.hdf5')
    layerName = [l.name for l in nn.layers if 'conv' in l.name][-1]
    print(nnC.evalModel(True))
    ag = Augmenter(numAugs, nn, layerName, augcamsz=nn.get_layer(layerName).output_shape[1:3], imgsz=(nnC.patientFileInfo['nchan'], nnC.sliceLengthSamples))
    highResCams = []
    sr = Superresolution(ag, 0.01)
    with alive_bar(len(sliceList)) as bar:
        for sliceIndex in sliceList:
            sr.runCamSection(nnC.xSlices[sliceIndex], nn=nn, cnnLayer=layerName)
            highResCams.append(sr.fullCam.numpy().squeeze().copy())
            bar()
    print('File used: {0}'.format(netToUse))
    return highResCams



if __name__ == '__main__':
    psr = argparse.ArgumentParser(description='This script uses a pretrained network and the augmenting script to run many augmented version of the data through the network in an attempt to figure out what the network learned.')

    psr.add_argument('patient', type=int, help='The patient to train the AI to predict seizures for')
    psr.add_argument('--uofr', type=bool, help='whether or not to use the U of R data for training.', required=False, default=False)
    psr.add_argument('--edf_start', help='EDF file to start with', default=0, type=int, required=False)
    psr.add_argument('--num_edfs', help='Number of EDF files to use', default=7, type=int, required=False)



    psr.add_argument('--net', help='The weights file for the neural network to use.', required=False, type=str, default=None)
    psr.add_argument('--of', help='Comments to put on the end of the file name', default='', type=str, required=False)
    psr.add_argument('--ratio', help='Ratio of preictal to non preictial', default=0.5, required=False)
    psr.add_argument('--augs', help='The number of augmentations of the high resolution CAM to to use.  Note larger numbers will take longer to run.', type=int, required=False, default=50)
    psr.add_argument('--slice', help='The index of the EEG slice to augment or a list of them.  \'all\' will augment all the slices.', required=False, default=0)
    psr.add_argument('--txt', required=False, default=False, action='store_true', dest='txtAlert', help='If added a text alert will be sent after training is done.')

    args = psr.parse_args()
    ret = main(netToUse=args.net,
               ratio=args.ratio,
               edfStart=args.edf_start,
               numEdfsToUse=args.num_edfs,
               patient=args.patient,
               numAugs=args.augs,
               sliceList=args.slice,
               uofr=args.uofr)
    import os
    path = os.getcwd()
    file = os.getcwd() + os.sep + args.of + '.pbz2'
    with bz2.BZ2File(file, 'wb') as f:
        pickle.dump(ret, f)
    if args.txtAlert:
        sendText('Finished Augmenting patient: {0}'.format(args.patient))
