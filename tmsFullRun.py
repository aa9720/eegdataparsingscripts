#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 21 11:07:19 2022

@author: allard
"""

from tmsAugment import main as augMain
from tmsTrain import main as trainMain
from tmsGradCam import gradCam
from tmsAugment import findWeightsFile
import argparse




if __name__ == '__main__':
    psr = argparse.ArgumentParser(description='''
This script starts the training for the CHB-MIT AI used to learn to predict patient specific seizures.

This script will train a neural net to classify pre-icatal data from non-preictal data within the CHB-MIT database.  This script has been tested for patients [1,3,7,21].

No analysis is done in the script on the NN or its post training performance.  Thus it is better to check the performance of the NN after training then select the

Beware: things like the augmenting output file can be overwritten.  There is no checking for file existance.

So...  You want to train an AI to classify preictal vs non-preictal?  Or are you looking to augment some data and see what an NN learned?  Eitherway, you are crazy!!!
Good luck to you!

''')

    psr.add_argument('task', type=str, help="What to do,\n\t'train' will run just the training script.\n\t'aug' will run just the augmenter script\n\t'both' will run both")

    psr.add_argument('patient', type=int, help='The patient to train the AI to predict seizures for')

    psr.add_argument('--edf_start', help='EDF file to start with (NOTE: 0 based indexing DEFAULT: 0)', default=0, type=int, required=False)
    psr.add_argument('--num_edfs', help='Number of EDF files to use', default=7, type=int, required=False)

    psr.add_argument('--bs', help='Batch size to be used. (DEFAULT: 64)', default=64, type=int, required=False)
    psr.add_argument('--eps', help='Number of Epochs to train the AI with. (DEFAULT: 500)', default=500, type=int, required=False)
    psr.add_argument('--fc', help='Comments to put on the end of the training weights file name', default='', type=str, required=False)
    psr.add_argument('--ratio', help='Ratio of preictal to non preictial for the training set', default=0.5, required=False)
    psr.add_argument('--doubleData', help='Whether or not to double the data array by flipping it around the time axis. (DEFAULT is false does not expect arg just a tag)', dest='doubleData', action='store_true',default=False)
    psr.add_argument('--quadData', help='Whether or not to qaudruple the data array by flipping it around the time axis, the channel axis and both (this will override doubleData). (DEFAULT is false does not expect arg just a tag)', dest='quadData', action='store_true',default=False)
    psr.add_argument('--uofr', type=bool, help='whether or not to use the U of R data for training.', required=False, default=False)
    psr.add_argument('--txt', required=False, default=False, action='store_true', dest='txtAlert', help='If added a text alert will be sent after training is done.  (note you will need to have setup a twilio account and setup all the necesary info it to send texts (SEE the txtAlert.py file for information)')


    psr.add_argument('--edf_start_aug', help='EDF file to start with for augmenting(NOTE: 0 based indexing DEFAULT: 0)', default=0, type=int, required=False)
    psr.add_argument('--num_edfs_aug', help='Number of EDF files to use for augmenting.  (This is the number of sequential EDFs from the edf_start_aug to use for augmenting. DEFAULT: 6)', default=6, type=int, required=False)



    psr.add_argument('--net', help='The weights file for the neural network to use for augmenting.  NOTE this will be interpreted as a regular expression (see python re docs for information on regular expressions)', required=False, type=str, default=None)
    psr.add_argument('--of', help='The file name for the augmented data to be written to (less the extension which will be .pbz2)', default='', type=str, required=False)
    psr.add_argument('--ratio_aug', help='Ratio of preictal to non preictial for augmenting data (because this is not training all data is fine default is \'all\'', default='all', required=False)
    psr.add_argument('--augs', help='The number of augmentations of the high resolution CAM to to use.  Note larger numbers will take longer to run. (DEFAULT is 50)', type=int, required=False, default=50)
    psr.add_argument('--slice', help='The index of the EEG slice to augment or a list of them.  \'all\' will augment all the slices. (DEFAULT is \'all\'.)', required=False, default='all')
    psr.add_argument('--txt_aug', required=False, default=False, action='store_true', dest='txtAlertAug', help='If added a text alert will be sent after training is done.')



    args = psr.parse_args()

    if args.task == 'both' or args.task == 'train' :
        ret = trainMain(eps=args.eps,
                        batchSize=args.bs,
                        fnCmts=args.fc,
                        ratio=args.ratio,
                        edfStart=args.edf_start,
                        numEdfsToUse=args.num_edfs,
                        patient=args.patient,
                        doubleData=args.doubleData,
                        quadrupleData=args.quadData,
                        textAlert=args.txtAlert,
                        uofr=args.uofr,
                        evalAllWeights=True)
    if args.task == 'both' or args.task == 'aug' :
        import os
        import numpy as np
        if args.net is None:

            netFile = ret[1]
        else:
            netFile = findWeightsFile(args.net)
        ret = augMain(netToUse=netFile,
                   ratio=args.ratio_aug,
                   edfStart=args.edf_start_aug,
                   numEdfsToUse=args.num_edfs_aug,
                   patient=args.patient,
                   numAugs=args.augs,
                   sliceList=args.slice,
                   uofr=args.uofr)
        print(netFile)
        import pickle
        from txtAlert import sendText
        import bz2
        path = os.getcwd()
        file = os.getcwd() + os.sep + args.of + '.pbz2'
        with bz2.BZ2File(file, 'wb') as f:
            pickle.dump(ret, f)
        if args.txtAlertAug:
            sendText('Finished Augmenting patient: {0}'.format(args.patient))
    if args.task == 'grad':
        import os
        import numpy as np
        netFile = findWeightsFile(args.net)
        ret = gradCam(netToUse=netFile,
                      ratio=args.ratio_aug,
                      edfStart=args.edf_start_aug,
                      numEdfsToUse=args.num_edfs_aug,
                      patient=args.patient,
                      sliceList=args.slice,
                      uofr=args.uofr)
        print(netFile)
        import pickle
        from txtAlert import sendText
        import bz2
        path = os.getcwd()
        file = os.getcwd() + os.sep + args.of + '.pbz2'
        with bz2.BZ2File(file, 'wb') as f:
            pickle.dump(ret, f)
