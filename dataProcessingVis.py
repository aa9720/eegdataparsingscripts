#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 11 11:38:26 2022

@author: allard
"""
# Unicode codes:
# Π = 3a0

# α = 3b1
# β = 3b2
# γ = 3b3
# δ = 3b4
# ε = 3b5
# ζ = 3b6
# η = 3b7
# θ = 3b8
# ι, κ = 3b9, 3ba
# λ = 3bb
# μ = 3bc
#ν, ξ, ο, π, ρ, ς, σ, τ, υ, φ, χ, ψ = [3bd...3c8]
# ω = 3c9
import pandas as pd


for patient in [1,3,21,7]:

    dropped = pd.read_csv('indexes/CHB_patient_{0}_indexesDropped.csv'.format(patient), skiprows=(1), names=['index', 'originalIndex'])
    kept = pd.read_csv('indexes/CHB_patient_{0}_indexesKept.csv'.format(patient), skiprows=(1), names=['index', 'originalIndex'])
    print('Number of epochs dropped {0} of {1} for Patient {2}.\nTotal trained on {3}'
          .format(dropped.shape[0],
                  dropped.shape[0]+kept.shape[0],
                  patient,
                  kept.shape[0]/0.5))
