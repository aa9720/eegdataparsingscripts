# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 19:31:10 2020

@author: aa9720
"""

from tools import getFilesByEXT
import pandas as pd
import mne
from mne.io import read_raw_edf
import keras
from eegParsing import tuhTseParser
import numpy as np
import random
import warnings
import os


class edfPreProcessor:

    def __init__(self, filePath=None):
        self._filePath = r'D:\tuhData\_DOCS'
        if filePath is not None:
            self._filePath = filePath
        self._xlsxFilePath = r'D:\tuhData\_DOCS\seizures_v34r.xlsx'

    def readSeizuresXlsx(self, filePath=None, sheetName='train'):
        if filePath is not None:
            self._xlsxFilePath = filePath
        return pd.read_excel(self._xlsxFilePath, sheetName, 0, skiprows=[1], index_col=0, usecols=range(15))



class dataGen(keras.utils.Sequence):
    def __init__(self, pathToFiles, batchSize, numDataIterations=1, seriesLen=1000):
        """
        Custom class to act as a keras generator to shell out the EEG data
        from the TUH dataset of EEG.

        Args:
            pathToFiles (STR): Location of the folder containing the eeg files
            batchSize (INT): number of slices of eeg data to put into the NN
            numDataIterations (INT, optional): Number of times to cycle
            through the data (__This should NOT be used__). Defaults to 1.
            seriesLen (TYPE, optional): DESCRIPTION. Defaults to 1000.

        Returns:
            None.

        """
        self.filePath = pathToFiles
        self.fileList = getFilesByEXT(pathToFiles, r'\.edf', True)
        print('Found {0} files'.format(len(self.fileList)))
        self.batchSize = batchSize
        self.numDataIterations = numDataIterations
        self.fileIndex = 0
        self.currentFileData = None
        self.cleanedData = None
        self.tseParser = tuhTseParser(pathToFiles)
        self.slidingWindow = True
        self.tseParsed = None
        self.outputs = None
        self.indexWithinFile = 0
        self.seriesLength = seriesLen
        self.preIctalBegin = -460800 # 30min
        self.postIctalEnd = 5000
        self.samplesPerFile = np.load(pathToFiles + '/samplesPerFile.npy')
        if (self.cleanedData is None) or (self.indexWithinFile + self.batchSize-1 + self.seriesLength >= self.cleanedData.shape[1]):
            self.loadEEGFile()
        self.calcLen()

    def on_epoch_end(self):
        # super(dataGen, self).on_epoch_end()
        listLen = len(self.fileList)
        shuffles = random.sample(range(listLen), listLen)
        self.fileList = [self.fileList[s] for s in shuffles]
        self.samplesPerFile = np.array([self.samplesPerFile[s] for s in shuffles])
        self.fileIndex = 0
        self.calcLen()

    def calcLen(self):
        self.size = int(np.floor(self.numDataIterations * self.samplesPerFile.sum() / self.batchSize))


    def __len__(self):
        return self.size

    def __genData(self):
        if (self.cleanedData is None) or (self.indexWithinFile + self.batchSize + self.seriesLength >= self.cleanedData.shape[1]):
            self.loadEEGFile()
        xs = [self.cleanedData[:, self.indexWithinFile + i:self.indexWithinFile + i + self.seriesLength] for i in range(self.batchSize)]
        # endIdxs = [self.indexWithinFile + i + self.seriesLength for i in range(self.batchSize)]
        ys = self.yData[self.indexWithinFile + self.seriesLength: self.indexWithinFile + self.seriesLength + self.batchSize, :]
        self.indexWithinFile += self.batchSize
        xs = np.array(xs).swapaxes(1,2).reshape((self.batchSize,self.seriesLength, 32, 1))  # for 2d cnn add ,1
        ys = np.array(ys)
        # print(ys.shape)
        # endIdxs = np.array(endIdxs)

        return xs, ys

    def __getitem__(self, index):
        x, y = self.__genData()
        return x, y

    def loadEEGFile(self, fileIndex=None, depth=None):
        self.indexWithinFile = 0
        if not fileIndex == None:
            self.fileIndex = fileIndex
        self.currentFileData = read_raw_edf(self.fileList[self.fileIndex], verbose=False)
        self.tseParsed = self.tseParser.parseFile(self.fileList[self.fileIndex], dropBackGround=True)
        if fileIndex == None:
            self.fileIndex += 1
            if self.fileIndex == len(self.fileList):
                self.fileIndex = 0
        if depth is not None:
            depth += 1
        else:
            depth = 0
        self.cleanData(depth)

    def cleanData(self, depth):
        dta = self.currentFileData.get_data([ch for ch in self.currentFileData.ch_names if not 'EKG' in ch])
        if dta.shape[0] < 32:
            dta = np.vstack((dta, dta[0:32-dta.shape[0],:]))
        elif dta.shape[0] > 32:
            dta = dta[0:32, :]
        dta = ((dta - dta.max()) / dta.std()) * 0.03
        self.cleanedData = dta
        self.outputs = np.zeros((dta.shape[1], 3))
        if self.tseParsed.shape[0] > 0:
            self.startIndexes = self.currentFileData.time_as_index(self.tseParsed['start'])
            self.endIndexes = self.currentFileData.time_as_index(self.tseParsed['stop'])
        else:
            self.startIndexes = None
            self.endIndexes = None
        # self.currentFileData = None
        ys = np.zeros((dta.shape[1], 3))
        if self.startIndexes is not None:

            for i in range(len(self.startIndexes)):
                ys[self.startIndexes[i]-self.preIctalBegin:self.startIndexes[i], 0] = 1
                ys[self.startIndexes[i]:self.endIndexes[i], 1] = 1
                ys[self.endIndexes[i]:self.endIndexes[i]+self.postIctalEnd, 2] = 1
                # ys[(endIdxs >= (self.startIndexes[i] + self.preIctalBegin)) &
                #    (endIdxs < self.endIndexes[i]), 0] = 1
                # ys[(endIdxs >= self.startIndexes[i]) &
                #    (endIdxs < self.endIndexes[i]), 1] = 1
                # ys[(endIdxs >= self.startIndexes[i]) &
                #    (endIdxs < (self.endIndexes[i] + self.postIctalEnd)), 2] = 1
            ys[ys[:, 1]==1, 0] = 0
            ys[ys[:, 1]==1, 2] = 0
        self.yData = ys
        if dta.max() > 1:
            # warnings.warn('Skipping file: {0}'.format(self.fileList[self.fileIndex]))
            if depth > 500:
                raise Exception('Max Depth reached')
            self.loadEEGFile(depth=depth)
