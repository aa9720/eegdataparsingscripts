# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 13:20:38 2020

@author: aa9720
"""
import os
import pandas as pd
import re
from tools import getFilesByEXT


class tuhParser:

    def __init__(self, path, ext='.edf'):
        self.extension = ext
        self.filePath = path
        self.fileList = getFilesByEXT(path, self.extension, True)
        self.selectedFileIndex = None

    def findFile(self, fileName):
        if isinstance(fileName, int):
            self.selectedFileIndex = fileName
        else:
            if fileName.find(self.extension) >= 0:
                fileName = fileName[:-4]
            for i in range(len(self.fileList)):
                if self.fileList[i].find(os.path.dirname(fileName)) >= 0:
                    break
            self.selectedFileIndex = i
        return self.fileList[self.selectedFileIndex]

    def openFile(self):
        os.startfile(self.fileList[self.selectedFileIndex])




class tuhTxtParser(tuhParser):
    def __init__(self, path):
        super().__init__(path, '\.txt')

    def parseFile(self, fileName=None):
        if fileName is not None:
            self.findFile(fileName)
        with open(self.fileList[self.selectedFileIndex]) as f:
            fileContent = f.read()
        age = int(re.findall('HISTORY:.+(\d+).y', fileContent)[0])
        history = fileContent.splitlines()[0].lower()
        meds = fileContent.splitlines()[1].lower()
        if meds == '':
            meds = fileContent.splitlines()[2].lower()
        if 'female' in history or 'woman' in history:
            man = False
        else:
            man = True
        meds = [m.replace(',', '') for m in meds.split(' ')[1:] if m != 'and']
        return {'age': age, 'male': man, 'meds': meds}


class tuhTseParser(tuhParser):
    def __init__(self, path):
        super().__init__(path, '\.tse')

    def parseFile(self, fileName=None, dropBackGround=False):
        if fileName is not None:
            self.findFile(fileName)
        info = pd.read_csv(self.fileList[self.selectedFileIndex], sep=' ', header=None, skiprows=[0,1])
        info.columns = ['start', 'stop', 'type', 'confidence']
        if dropBackGround:
            info = info[info['type'] != 'bckg']
        return info