# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 10:05:21 2020

@author: aa9720

An NN script Allard wrote to start working on the TUH database.

"""
from keras.layers import Dense, Dropout, LSTM, Conv1D, Flatten, MaxPooling1D, Embedding, SpatialDropout1D
from tensorflow.compat.v1.keras.layers import CuDNNLSTM
from keras.layers import Conv2D, Flatten, MaxPooling2D, Embedding, SpatialDropout2D, Bidirectional, TimeDistributed, Reshape, GRU, RNN
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint
from eegEDFParsing import dataGen
import os

batchSize = 1000
seriesLen = 1280

dGen = dataGen(pathToFiles=os.path.expanduser('~/eegData/eegdata/edf/train'),
               batchSize=batchSize,
               seriesLen=seriesLen,
               numDataIterations=1)




model = Sequential()
model.add(Conv2D(32, kernel_size=(3,2), strides=(1,1), input_shape=[seriesLen, 32, 1], activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
# model.add(Dropout(0.2))
model.add(Conv2D(32, kernel_size=(3,2), strides=(1,1), activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
# model.add(Dropout(0.1))
model.add(Conv2D(32, kernel_size=(3,2), strides=(1,1), activation='relu'))
# model.add(Dropout(0.3))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(32, kernel_size=(3,2), strides=(1,1), activation='relu'))
# model.add(Dropout(0.2))
model.add(Reshape((-1,32)))
if not os.uname()[1] == 'automation':
    model.add(Bidirectional(CuDNNLSTM(20)))
else:
    model.add(Bidirectional(LSTM(20)))
# model.add(Flatten())
# model.add(Dense(20, activation='relu'))

# model.add(Dense(units=128, activation='relu'))
# model.add(Dense(units=64, activation='relu'))
# model.add(Dense(units=32, activation='relu'))
# model.add(Dropout(0.2))
model.add(Dense(units=3, activation='sigmoid'))

model.compile('adam',
              'binary_crossentropy',
              metrics=['accuracy'])

filepath="weights-improvement-{epoch:02d}-{val_accuracy:.2f}.hdf5"

cp = ModelCheckpoint(filepath, monitor='val_accuracy', save_best_only=True, mode='max')

model.fit(dGen, workers=16, epochs=10, max_queue_size=20)



#%% Testing

dGen2 = dataGen(pathToFiles=os.path.expanduser('~/eegData/eegdata/edf/dev'),
               batchSize=batchSize,
               seriesLen=seriesLen,
               numDataIterations=1)


predictions = model.predict(dGen2)
