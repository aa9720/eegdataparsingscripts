#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 08:45:49 2021

@author: allard

This class is setup to be a single file class for all of the AI parsing for the
CHBMIT data.  This will hopfully reduce the number of odd files in use as well
as be more efficient.


Hello there.

I know not how long it has been since this code was last looked at or modified.  My guess is it has been quite awhile.  You can check the gitlab commit log to see how long it has been.  I guess the first question you will probably as is why is this setup this way?  Well, it really should be more spread out across several other files and classes.  But time was not on my side.  I ended up allowing this to grow and then when I realized I was should split it off and make use of proper inheritance.... I didn't have too long before I was supposed to defend my MSEE thesis.  So I left it as is.  It does make it nice as most everything you could ever want is tucked away in this file for the NN and the augmenting stuff is in one other file, but it is a bit clustered in here.

If you don't mind please fill out the info below so we can track who has worked on this file in the future:  (please copy the template and leave the template in place)


================Authors===========================

Name: Andrew Allard
Project: Masters thesis (spring 2022)
Edits: Wrote the entire files
Start Date: ~Fall 2020
End Date: April 15 2022
git tag (if applicable): defense
notes: Wrote the file to parse patients 1,3,7,21 for CHB and patients 1 and 6 from the UR data.

________________Template____________________
Name:
Project:
Edits:
Start Date:
End Date:
git tag (if applicable):
notes:

"""

import platform
import os
import numpy as np
# Custom tools to process
from tools import parseCHBpatient, eegPathByMachine, getFilesByEXT

# Tensorflow stuff for the neural network
from tensorflow.keras.models import Model
from tensorflow.version import VERSION as TF_VERSION
from tensorflow.keras.layers import Dense, Dropout, LSTM, Conv2DTranspose, Flatten, MaxPooling1D, Embedding, SpatialDropout1D, ReLU, LeakyReLU
from tensorflow.compat.v1.keras.layers import CuDNNLSTM
from tensorflow.keras.layers import Conv2D, Flatten, MaxPooling2D, Embedding, SpatialDropout2D, Bidirectional, TimeDistributed, Reshape, GRU, RNN, BatchNormalization, UpSampling2D, Activation, ZeroPadding2D
from tensorflow.keras.models import Sequential
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.optimizers import Adam, RMSprop
from tensorflow.keras.regularizers import l1, l2, l1_l2
from tensorflow.keras.layers import LeakyReLU

# used for debugging and other stuff
import warnings
import gc
# main processing libs
import mne
import datetime
import pandas as pd

# Added after thesis for better more automated evaluation of NNs
from sklearn.metrics import roc_curve, auc

tfVerFlag = int(TF_VERSION.split('.')[1]) > 6

class customCP(ModelCheckpoint):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.listOfFileNames = []

    def on_epoch_end(self, epoch, logs=None):
        super().on_epoch_end(epoch, logs)
        if tfVerFlag:
            filepath = self._get_file_path(epoch, batch=None, logs=logs)
        else:
            filepath = self._get_file_path(epoch, logs=logs)
        current = logs.get(self.monitor)
#        if self.monitor_op(current, self.best):
        self.listOfFileNames.append(filepath)



class nerualNet:
    _hostNums = {'automation': 0,
                 'Allard-PC': 1,
                 'kgcoe-cuda-05': 2,
                 'kgcoe-cuda-04': 3,
                 'GLE-3241-RA03': 4,
                 'unknown': 5,
                 'deepEEG': 6,
                 'eegNeuralNets': 7}


    def __init__(self, patient:int=1):
        """
        Class

        Parameters
        ----------
        patient : INT, optional
            The CHB-MIT patient you wish to train on. The default is 1.
            This has been tested with patients 1, 3, 7, 21.  Others may require
            Modification to get operating corretly.

        Returns
        -------
        None.

        """
        self._host = platform.node()
        if self._host in self._hostNums.keys():
            self._hostNum = self._hostNums[self._host]
        else:
            self._hostNum = self._hostNums['unknown']
            warnings.warn('I do not know the machine you are trying to run on.')
        self.currentPatient = patient
        self.numEDFsToUse = 16
        self.edfStartingFile = 0
        self.setupHost()
        self._infoLoaded = False
        self._dataLoaded = False
        self._dir_path = os.path.dirname(os.path.realpath(__file__))
        self.initVars()


    def initVars(self):
        """
        A simple function called during initialization automatically
        Used to to setup to some variables specific to the ictal windows

        Returns
        -------
        None.

        """
        # vars
        self.preIctalWindow = 60*60 # In seconds
        self.interIctalWindow = 60*60
        self.postIctalWindow = 60*60*4

        self.sliceLength = 10 # in seconds Changed 1/15/22 from 5 and back to 5 from 1 on 1/20/22
        self.numOutputNerons = 1
        self.sliceLengthSamples = 1280 # (a guess)
        self.fileLengths = []
        self.patientFileInfo = None
        self._preFileName = 'CHB'
        self._chanExcluded = ['_','-']


    def setupHost(self):
        """
        A simple function called during startup automatically.  This one reads
        the host computer name and customizes variables for that host.
        If the host you are operating on is not in _hostNums you will need to
        add it for this function to modify things for a host.

        Returns
        -------
        None.

        """
        if self._hostNum == 1:
            # laptop GPU doesn't have enough memory so we don't want to use it.
            pass
            # os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
        if not self._hostNum == 2:
            self.numEDFsToUse = 7

    def loadCHBdataInfo(self):
        warnings.warn('Deprecated: Use loadDataInfo instead')
        return self.loadDataInfo()

    def loadCHBdata(self, append=False):
        warnings.warn('Deprecated: Use loadData instead')
        return self.loadData(append=append)

    def loadDataInfo(self):
        """
        Loads in the CHB-MIT data information.  This requires the tools.py
        collection of functions to be in your python path.

        Returns
        -------
        None.

        """
        self._edfs, self._fileInfo, self._info = parseCHBpatient(self.currentPatient)
        self._infoLoaded = True
        data = mne.io.read_raw_edf(self._edfs[0], exclude='-')
        self.patientFileInfo = data.info.copy()

    def loadData(self, append:bool=False):
        if not self._infoLoaded and not append:
            self.loadDataInfo()
        if not append:
            self.fileLengths = []
        arrayForDL = np.ndarray((23,0))
        indexList = [0]
        labelList = []
        fileIndex = self.edfStartingFile
        for edf in self._edfs[self.edfStartingFile:self.edfStartingFile+self.numEDFsToUse]:
            data = mne.io.read_raw_edf(edf, preload=True, exclude='-')
            badChans = [c for c in data.ch_names if c[0] == '-']
            if not badChans == []:
                data = data.drop_channels(badChans)
            dt = data.get_data()
            self.fileLengths.append(dt.shape[1])
            arrayForDL = np.append(arrayForDL, dt, axis=1)
            labelList.extend(np.zeros((dt.shape[1])))
            indexList.append(dt.shape[1])
            if fileIndex < len(self._edfs) - 1:
                arrayForDL = np.append(arrayForDL, np.zeros((dt.shape[0], int((self._fileInfo[fileIndex+1]['sTime'] - self._fileInfo[fileIndex]['eTime']).seconds * data.info['sfreq']))), axis=1)
                indexList.append(len(np.zeros((int((self._fileInfo[fileIndex+1]['sTime'] - self._fileInfo[fileIndex]['eTime']).seconds * data.info['sfreq'])))))
                labelList.extend(np.zeros((int((self._fileInfo[fileIndex+1]['sTime'] - self._fileInfo[fileIndex]['eTime']).seconds * data.info['sfreq']))))
                fileIndex += 1
            elif fileIndex == len(self._edfs):
                self.numEDFsToUse = fileIndex - self.edfStartingFile - 1

        labelList = np.array(labelList)
        preIctalWindowSamples = int(self.preIctalWindow * data.info['sfreq'])
        interIctalWindowSamples = int(self.interIctalWindow * data.info['sfreq'])
        postIctalWindowSamples = int(self.postIctalWindow * data.info['sfreq'])
        self.sliceLengthSamples = int(self.sliceLength * data.info['sfreq'])

        sFreq = data.info['sfreq']

        indexList = np.cumsum(indexList)
        outputLables = np.zeros((3,len(labelList)))

        for f in range(self.numEDFsToUse):
            if (f + self.edfStartingFile) >= (len(self._edfs) - 1):
                continue
            if self._fileInfo[f + self.edfStartingFile]['numSeizures'] > 0:
                for seizure in self._fileInfo[f + self.edfStartingFile]['seizureList']:
                    startIndex = int(seizure['sTime'] * sFreq)
                    endIndex = int(seizure['eTime'] * sFreq)
                    outputLables[0, indexList[f*2]+startIndex - preIctalWindowSamples:indexList[f*2]+startIndex-1] = 1 # preictal
                    outputLables[1, indexList[f*2]+startIndex:indexList[f*2]+endIndex] = 1 # ictal
                    outputLables[2, indexList[f*2]+endIndex:indexList[f*2]+endIndex+postIctalWindowSamples] = 1 # postictal/interictal

        outputLables = outputLables[:, (arrayForDL!=0).max(0)]
        labelList = labelList[(arrayForDL!=0).max(0)]

        arrayForDL = arrayForDL[:, (arrayForDL!=0).max(0)]

        xSlices = []
        ySlices = []

        for sliceIndex in range(0, (arrayForDL.shape[1] - arrayForDL.shape[1] % self.sliceLengthSamples), self.sliceLengthSamples):
            xSlices.append(arrayForDL[:,sliceIndex:sliceIndex+self.sliceLengthSamples])
            ySlices.append(outputLables[:,sliceIndex+self.sliceLengthSamples - 1])

        del arrayForDL
        gc.collect()

        if not append:
            self.xSlices = np.array(xSlices).reshape((len(xSlices), 23,-1, 1)).copy()
            self.ySlices = np.array(ySlices).reshape((len(xSlices), -1)).copy()
        else:
            print("appending data")
            xSlices = np.array(xSlices).reshape((len(xSlices), 23,-1, 1))
            ySlices = np.array(ySlices).reshape((len(xSlices), -1))
            self.xSlices = np.append(self.xSlices, xSlices, axis=0)
            self.ySlices = np.append(self.ySlices, ySlices, axis=0)
        del xSlices
        del ySlices
        self._dataLoaded = True

    def dropExcessData(self, ratio:float=0.5, dropToFile:bool=True, repeat:bool=False):
        toDropIndexes = (self.ySlices[:, 0] != 1) & (self.ySlices[:, 1] != 1)
        if not repeat:
            # This is really the number of preictal points within the EEG epochs.
            # But Seiz is faster to type than preictal
            numSeizPoints = len(self.ySlices[~toDropIndexes, 0])
            # again preictal but seiz is faster to type during coding
            numNonSeizPoints = len(self.ySlices[toDropIndexes, 0])
            currentRatio = numSeizPoints / len(self.ySlices)
            if currentRatio > ratio:
                # If the current ratio is already higher than the desired ratio
                # We raise an error to be sure the user is well aware of their
                # accident.
                raise Exception('There is already a higher ratio of preictal')
            diff = ratio - currentRatio
            allIndexes = np.arange(numNonSeizPoints)
            leftOver = np.random.choice(allIndexes,
                                     int(round(numSeizPoints / ratio * (1 - ratio))))
            leftOver.sort()
            dropped = allIndexes[~np.in1d(allIndexes, leftOver)]
        else:
            # This option allows for dropping indexes from the previous run.
            # This is useful if one would like to check repeatability.
            warnings.warn('ratio ignored as this is a repeat')
            leftOver = np.int64(np.loadtxt('indexes/{1}_patient_{0}_indexesKept.csv'.format(self.currentPatient, self._preFileName), float))
            dropped = np.loadtxt('indexes/{1}_patient_{0}_indexesDropped.csv'.format(self.currentPatient, self._preFileName), float)
        xSlicesNoSeizure = self.xSlices[toDropIndexes, :, :, :][leftOver, :, :, :]
        ySlicesNoSeizure = self.ySlices[toDropIndexes, :][leftOver, :]
        if dropToFile and not repeat:
            if not os.path.exists('indexes'):
                os.mkdir('indexes')
            np.savetxt('indexes/{1}_patient_{0}_indexesKept.csv'.format(self.currentPatient, self._preFileName),
                       leftOver,
                       delimiter=',',
                       header='# starting: {0} new: {1}'.format(currentRatio, ratio))
            np.savetxt('indexes/{1}_patient_{0}_indexesDropped.csv'.format(self.currentPatient, self._preFileName),
                       dropped,
                       delimiter=',',
                       header='# starting: {0} new: {1}'.format(currentRatio, ratio))

        self.xSlices = np.append(xSlicesNoSeizure, self.xSlices[self.ySlices[:,0] == 1, :], axis=0)
        self.ySlices = np.append(ySlicesNoSeizure, self.ySlices[self.ySlices[:,0] == 1, :], axis=0)



    def showDataStats(self):
        print("Total number of points: {0}\nTotal preictal points: {1}".format(len(self.ySlices),
                                                                               len(self.ySlices[~((self.ySlices[:, 0] != 1) & (self.ySlices[:, 1] != 1)),0])))

    def buildNN(self):
        self.model = Sequential()
        # self.model.add(BatchNormalization(axis=[-2]))
        self.model.add(Conv2D(32,
                              kernel_size=(2,3),
                              strides=(1,1),
                              input_shape=(self.xSlices.shape[1],
                                           self.sliceLengthSamples,1),
                              activation=LeakyReLU()))
        self.model.add(BatchNormalization(axis=[-1]))
        self.model.add(Conv2D(32,
                              kernel_size=(2,2),
                              strides=(2,2)))
        # self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(BatchNormalization(axis=[-1]))
        # self.model.add(Dropout(0.15))

        self.model.add(Conv2D(32, kernel_size=(2,3), strides=(1,1), activation=LeakyReLU()))
        self.model.add(BatchNormalization(axis=[-1]))
        self.model.add(Conv2D(32,
                              kernel_size=(2,2),
                              strides=(2,2)))
        # self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(BatchNormalization(axis=[-1]))
        # self.model.add(Dropout(0.2))

        self.model.add(Conv2D(32, kernel_size=(2,3), strides=(1,1), activation=LeakyReLU()))
        self.model.add(BatchNormalization(axis=[-1]))
        self.model.add(Conv2D(32,
                              kernel_size=(2,2),
                              strides=(2,2)))
        # self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(BatchNormalization(axis=[-1]))
        if self.patientFileInfo['nchan'] == 23:
            self.model.add(Conv2D(32, kernel_size=(2,3), strides=(1,1), activation=LeakyReLU()))
        else:
            self.model.add(Conv2D(32, kernel_size=(1,3), strides=(1,1), activation=LeakyReLU()))
        self.model.add(Reshape((-1,32)))
        self.model.add(BatchNormalization(axis=[-1]))
        self.model.add(Dropout(0.1))
        self.model.add(Bidirectional(LSTM(20,
                                          activation='tanh',
                                          recurrent_activation='sigmoid',
                                          recurrent_dropout=0,
                                          unroll=False,
                                          use_bias=True,
                                          recurrent_regularizer=l2(),
                                          kernel_regularizer=l2())))

        self.model.add(Dense(units=self.numOutputNerons, activation='sigmoid'))

        self.model.compile(optimizer=Adam(learning_rate=0.0001,
                                          amsgrad=True,
                                          epsilon=3e-7),
                           loss='binary_crossentropy',
                           metrics=['accuracy'])
        return self.model

    def buildConstuctor(self):
        self.gen = Sequential()
        self.gen.add(Conv2DTranspose(32, kernel_size=(2,3), strides=(1, 1), input_shape=(1, 156, 32)))
        self.gen.add(BatchNormalization(axis=[-2], momentum=0.8))
        self.gen.add(Conv2DTranspose(32,
                                     kernel_size=(2,2),
                                     activation='relu',
                                     strides=(2, 2)))
        self.gen.add(BatchNormalization(axis=[-2], momentum=0.8))
        self.gen.add(Conv2DTranspose(32, kernel_size=(2,3), strides=(1, 1), activation='relu'))
        self.gen.add(BatchNormalization(axis=[-2], momentum=0.8))
        self.gen.add(Conv2DTranspose(32,
                                     kernel_size=(2,2),
                                     activation='relu',
                                     strides=(2, 2)))
        self.gen.add(ZeroPadding2D(padding=((0,0), (0, 1))))
        self.gen.add(BatchNormalization(axis=[-2], momentum=0.8))
        self.gen.add(Conv2DTranspose(32, kernel_size=(2,3), strides=(1, 1), activation='relu'))
        self.gen.add(BatchNormalization(axis=[-2], momentum=0.8))
        self.gen.add(Conv2DTranspose(32,
                                     kernel_size=(2,2),
                                     activation='relu',
                                     strides=(2, 2)))
        self.gen.add(BatchNormalization(axis=[-2], momentum=0.8))
        self.gen.add(Conv2DTranspose(1, kernel_size=(2,3), strides=(1, 1), activation='sigmoid'))
        self.gen.add(BatchNormalization(axis=[-2], momentum=0.8))
        self.gen.add(Conv2DTranspose(1, kernel_size=(1,1), strides=(1, 1), activation='sigmoid'))
        # self.gen.add(Conv2DTranspose(1, kernel_size=(1,1), strides=(1, 1), activation='sigmoid'))
        # self.gen.compile('rmsprop',
        #                  'binary_crossentropy',
        #                  metrics=['accuracy'])
        return self.gen

    def setGenWeightsFromNN(self):
        for i in range(13):
            if i < 7:
                self.gen.layers[i].set_weights(self.model.layers[-6-i].get_weights())
            elif i > 7:
                self.gen.layers[i].set_weights(self.model.layers[-5-i].get_weights())
        ws = self.model.layers[1].get_weights()
        self.gen.layers[13].set_weights([ws[0], ws[1][0:1]])
        self.gen.layers[14].set_weights(self.model.layers[0].get_weights())

    def buildGAN(self):
        if self._hostNum == 2:
            self.desc = Model(inputs=self.model.layers[0].input,
                              outputs=self.model.layers[-6].output,
                              )
        else:
            self.desc = Model(inputs=self.model.layers[0].input,
                              outputs=self.model.layers[-6].output,
                              )
        self.normalizer = Model(inputs=self.model.layers[0].input,
                          outputs=self.model.layers[0].output)
        self.GAN = Sequential()
        self.GAN.add(self.desc)
        self.GAN.add(self.gen)
        self.GAN.layers[0].trainable = False
        self.GAN.compile('adam',
                         'binary_crossentropy',#'binary_crossentropy',
                         metrics=['accuracy'])
        return self.GAN

    def normalizeXSlices(self):
        self.normedX = self.normalizer(self.xSlices)

    def trainModel(self, fnComments='', doubleDataSize=False, quadDataSize=False, **kwargs):
        if not self._dataLoaded:
            self.loadCHBdata()
        self._filepath=self._dir_path+"/savedWeights/{3}_patient{0}-{1}-ep{{epoch:02d}}-ls{{loss:.5f}}-ac{{accuracy:.5f}}_{2}.h5".format(self.currentPatient, self._host, fnComments, self._preFileName)
        cp = customCP(filepath=self._filepath, monitor='loss', save_best_only=True, mode='min', verbose=1)
        if doubleDataSize or quadDataSize:
            self.xSlices = np.append(self.xSlices, self.xSlices[:,:,::-1,:], axis=0)
            self.ySlices = np.tile(self.ySlices, [2, 1])
            if quadDataSize:
                self.xSlices = np.append(self.xSlices, self.xSlices[:,::-1,:,:], axis=0)
                self.ySlices = np.tile(self.ySlices, [2, 1])
        ret = self.model.fit(x=self.xSlices, y=self.ySlices[:, 0:self.numOutputNerons], callbacks=cp, **kwargs)
        return ret, cp.listOfFileNames.copy()


    def evalModel(self, full:bool=False, binaryThresh:float=0.9, printRes:bool=True, calcAUC:bool=False):
        firstPreict = np.where(self.ySlices[:,0]==1)[0][0]
        # The following two lines use, max in cse for some reason the pre ictal
        # starts at the begininning of the file in which case this would
        # evaluate to a negative index and therefore pass a 0 width array into
        # the NN.  This is a bad thing...
        if printRes:
            print(np.array([self.model.predict(self.xSlices[max(firstPreict-10,0):firstPreict+10]).squeeze(),
                            self.ySlices[max(firstPreict-10,0):firstPreict+10, :self.numOutputNerons].squeeze()]))
        if full:
            preds = self.model.predict(self.xSlices)
            analogPreds = preds.copy()
            preds[preds >= binaryThresh] = 1
            preds[preds < binaryThresh] = 0
            yPs = (self.ySlices[:,0]==1).sum(0)
            yNs = (self.ySlices[:,0]==0).sum(0)
            oPs = (preds[self.ySlices[:,0]==1] == 1).sum(0)
            oNs = (preds[self.ySlices[:,0]==0] == 0).sum(0)
            spec = oNs / yNs
            sens = oPs / yPs
            if not calcAUC:
                return sens, spec
            fpr, tpr, _ = roc_curve(self.ySlices[:,0], analogPreds)
            area = auc(fpr, tpr)
            return area, sens, spec

    def loadFileInfo(self, fNum:int=0):
        return mne.io.read_raw_edf(self._edfs[fNum], exclude=self._chanExcluded).info

    def xArToEpochs(self, infoFileNum:int=0):
        return mne.EpochsArray(self.xSlices.squeeze(), info=self.loadFileInfo(infoFileNum))

    # def showConvLayerWeights(self, layer=1):
    #     ws, bs = self.model.layers[layer].get_weights()
    #     pass


class neuralNetUR(nerualNet):

    def __init__(self, patient:int=1):
        """
        This is the child class for the UR data.  This class wraps the UR data
        up so it can be used similar to the CHB-MIT data.

        Parameters
        ----------
        patient : int, optional
            DESCRIPTION. The default is 1.

        Returns
        -------
        None.

        """
        super().__init__(patient=patient) # Intitalize the parent class
        # A couple flags used to differentiate between UR and CHB
        self.UR_mode = True
        self._preFileName = 'UR'
        # Some random defaults
        self.numEDFsToUse = 3
        self.edfStartingFile = 0
        # A bunch of channels in the UR data set are not common between patients
        # This is a list of the channels to drop as they are commone.
        self._chanExcluded = ['EMG1', 'EMG2', 'EMG3', 'EMG4',
                              'ECG1', 'ECG2', 'ECG3', 'ECG4',
                              '-',
                              'FT9', 'FT10',
                              'X28', 'X29', 'X30',
                              'EMG3', 'EMG4',
                              'ECG-1', 'ECG-2', 'ECG-LL', 'ECG-3', 'ECG-4',
                              'EOG-L', 'EOG-R',
                              'Oz']
        self.annotationList = []


    def loadDataInfo(self):
        self._fileInfo, self._info = None, None
        path = eegPathByMachine() + 'UR-EEG-Database-RIT'
        self._edfs = getFilesByEXT(path, '{0:02}\w.edf\Z'.format(self.currentPatient))
        self._edfs.sort()
        self._infoLoaded = True
        data = mne.io.read_raw_edf(self._edfs[0], exclude=self._chanExcluded)
        self.patientFileInfo = data.info.copy()

    def loadData(self, append:bool=False):
        xSlices, ySlices = self._iterFiles(append)
        gc.collect()
        if not append:
            self.xSlices = np.array(xSlices).reshape((
                len(xSlices),
                self.patientFileInfo['nchan'],
                -1,
                1)).copy()
            self.ySlices = np.array(ySlices).reshape((len(xSlices), -1)).copy()
        else:
            print("appending data")
            xSlices = np.array(xSlices).reshape((len(xSlices),
                                                 self.patientFileInfo['nchan'],
                                                 -1,
                                                 1))
            ySlices = np.array(ySlices).reshape((len(xSlices), -1))
            self.xSlices = np.append(self.xSlices, xSlices, axis=0)
            self.ySlices = np.append(self.ySlices, ySlices, axis=0)
        del xSlices
        del ySlices
        self._dataLoaded = True


    def _iterFiles(self, append:bool=False):
        self.annotationList = []
        gc.collect()
        if not self._infoLoaded and not append:
            self.loadDataInfo()
        if not append:
            self.fileLengths = []
        arrayForDL = np.ndarray((self.patientFileInfo['nchan'],0))
        indexList = [0]
        labelList = []
        self._measDates = []
        fileIndex = 0 #self.edfStartingFile


        for edf in self._edfs[self.edfStartingFile:self.numEDFsToUse+self.edfStartingFile]:
            data = mne.io.read_raw_edf(edf, preload=True, exclude=self._chanExcluded)
            sFreq = data.info['sfreq']
            self._measDates.append(data.info['meas_date'])
            badChans = [c for c in data.ch_names if c[0] == '-']
            if not badChans == []:
                data = data.drop_channels(badChans)
            dt = data.get_data()
            self.annotationList.append(data.annotations.to_data_frame())
            self.fileLengths.append(dt.shape[1])
            if fileIndex > 0:
                tdelta = self._measDates[-1].timestamp() * sFreq - self.fileLengths[-2] - self._measDates[-2].timestamp() * sFreq
                arrayForDL = np.concatenate((arrayForDL, np.zeros((self.patientFileInfo['nchan'], int(round(tdelta))))), axis=1)
                indexList.append(len(np.zeros((int(round(tdelta))))))
                labelList.extend(np.zeros((int(round(tdelta)))))
            arrayForDL = np.concatenate((arrayForDL, dt), axis=1)
            gc.collect()
            labelList.extend(np.zeros((dt.shape[1])))
            indexList.append(dt.shape[1])

            if fileIndex < len(self._edfs) - 1:
                fileIndex += 1
            elif fileIndex > len(self._edfs):
                self.numEDFsToUse = fileIndex - self.edfStartingFile

        labelList = np.array(labelList)
        preIctalWindowSamples = int(self.preIctalWindow * data.info['sfreq'])
        # interIctalWindowSamples = int(self.interIctalWindow * data.info['sfreq'])
        postIctalWindowSamples = int(self.postIctalWindow * data.info['sfreq'])
        self.sliceLengthSamples = int(self.sliceLength * data.info['sfreq'])

        indexList = np.cumsum(np.array(indexList))
        outputLables = np.zeros((3,len(labelList)))

        for f in range(fileIndex):
            starts = self.annotationList[f][self.annotationList[f]['description'] == 'SEIZURE ONSET']
            if starts.shape[0] < 1:
                starts = self.annotationList[f][self.annotationList[f]['description'] == 'Seizure']
            ends = self.annotationList[f][self.annotationList[f]['description'] == 'SEIZURE END']
            ts = self._measDates[f].timestamp()
            for i in range(starts.shape[0]):
                startIndex = int(round((pd.to_datetime(starts['onset'].values[i]).timestamp() - ts) * sFreq))
                if ends.shape[0] < starts.shape[0]:
                    if starts['duration'].values[i] > 0:
                        endIndex = int(round(startIndex + starts['duration'].values[i] * sFreq))
                    else:
                        endIndex = int(round(startIndex + 30 * sFreq))
                else:
                    endIndex = int(round((pd.to_datetime(ends['onset'].values[i]).timestamp() - ts) * sFreq))
                outputLables[0, max(0, indexList[f*2]+startIndex - preIctalWindowSamples):indexList[f*2]+startIndex-1] = 1 # preictal
                outputLables[1, indexList[f*2]+startIndex:indexList[f*2]+endIndex] = 1 # ictal
                outputLables[2, indexList[f*2]+endIndex:indexList[f*2]+endIndex+postIctalWindowSamples] = 1 # postictal/interictal



        # This part is autonomous and independent of data.
        outputLables = outputLables[:, (arrayForDL!=0).max(0)]
        labelList = labelList[(arrayForDL!=0).max(0)]

        arrayForDL = arrayForDL[:, (arrayForDL!=0).max(0)]

        xSlices = []
        ySlices = []

        for sliceIndex in range(0, (arrayForDL.shape[1] - arrayForDL.shape[1] % self.sliceLengthSamples), self.sliceLengthSamples):
            xSlices.append(arrayForDL[:,sliceIndex:sliceIndex+self.sliceLengthSamples])
            ySlices.append(outputLables[:,sliceIndex+self.sliceLengthSamples - 1])

        del arrayForDL
        del outputLables
        del labelList
        del data
        gc.collect()
        return xSlices, ySlices
