#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 13:36:43 2021

@author: allard
"""


import matplotlib.pyplot as p
import numpy as np
from nerualNetViewer import get_activations
import tensorflow as tf
import imutils



startIdx = 1746

acts = get_activations(nn, nnC.xSlices[startIdx:startIdx+1])


features = acts['conv2d_3'].squeeze().T

spect = np.abs(np.fft.fftshift(np.fft.fft2(features)))
spectPhase = np.angle(np.fft.fftshift(np.fft.fft2(features)))

f, sbps = p.subplots(6,6)

for s in range(len(spect)):
    sbps.flatten()[s].semilogy(spect[s,:])
    sbps.flatten()[s].grid(True)



def make_gradcam_heatmap(img_array, model, last_conv_layer_name, pred_index=None):
    # First, we create a model that maps the input image to the activations
    # of the last conv layer as well as the output predictions
    grad_model = tf.keras.models.Model(
        [model.inputs], [model.get_layer(last_conv_layer_name).output, model.output]
    )

    # Then, we compute the gradient of the top predicted class for our input image
    # with respect to the activations of the last conv layer
    with tf.GradientTape() as tape:
        last_conv_layer_output, preds = grad_model(img_array)
        if pred_index is None:
            pred_index = tf.argmax(preds[0])
        class_channel = preds[:, pred_index]

    # This is the gradient of the output neuron (top predicted or chosen)
    # with regard to the output feature map of the last conv layer
    grads = tape.gradient(class_channel, last_conv_layer_output)

    # This is a vector where each entry is the mean intensity of the gradient
    # over a specific feature map channel
    pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))

    # We multiply each channel in the feature map array
    # by "how important this channel is" with regard to the top predicted class
    # then sum all the channels to obtain the heatmap class activation
    last_conv_layer_output = last_conv_layer_output[0]
    heatmap = last_conv_layer_output @ pooled_grads[..., tf.newaxis]
    heatmap = tf.squeeze(heatmap)

    # For visualization purpose, we will also normalize the heatmap between 0 & 1
    heatmap = tf.maximum(heatmap, 0) / tf.math.reduce_max(heatmap)
    return heatmap.numpy()



def augmentEEG(eeg, theta=0, x=0, y=0):
    return imutils.rotate(imutils.translate(eeg, x, y), theta)

def rollEEG(eeg, x=0, y=0, wrap=False):
    mod = eeg.copy()
    if eeg.ndim > 2:
        axisOffset = 1
        if not wrap:
            if y != 0:
                mod[:,-y:,:,:]=0
            if x != 0:
                mod[:,:,-x:,:]=0
    else:
        axisOffset = 0
        if not wrap:
            if y != 0:
                mod[-y:,:]=0
            if x != 0:
                mod[:,-x:]=0
    return np.roll(mod, (x,y), (1+axisOffset, 0+axisOffset))

