#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 21 11:22:53 2022

@author: allard
"""

import matplotlib.pyplot as p
import numpy as np
import pickle
from matplotlib.animation import FuncAnimation, FFMpegWriter


with open('initialTestAugment2.pkl','rb') as f:
    augs = pickle.load(f)


augs = np.array(augs).swapaxes(0,1).reshape(23,-1)

egData = nnC.xSlices.copy().squeeze().swapaxes(0,1).reshape(23,-1)

nnC=0
#%% animate



fg, ax = p.subplots()
fg.set_tight_layout(True)
chNames = nnC.loadFileInfo()['ch_names']
yArC = np.arange(0, 24)-0.5
xArC = np.linspace(0, nnC.sliceLength, nnC.sliceLengthSamples+1)-0.5/nnC.sliceLengthSamples*nnC.sliceLength

yAr = np.arange(0, 23)
xAr = np.linspace(0, nnC.sliceLength, nnC.sliceLengthSamples)
fullX = np.tile(xAr.reshape(nnC.sliceLengthSamples,1), (1,23))
egOffsets = np.tile(yAr.reshape((23,1)), (1,nnC.sliceLengthSamples)).T

rat = 5/1280

egData -= egData.mean()
sc = p.plot(fullX,
            egData[:,:1280].copy().T,
            lw=0.5,
            c='k',
            alpha=1)
X,Y = np.meshgrid(xArC, yArC)
pcMesh = fg.get_axes()[0].pcolormesh(X,Y,augs[:,:1280], cmap='jet', alpha=0.7)



def updateFig(frame):
    ax.clear()
    agSlice = augs[:,frame:frame+1280].copy()
    egSlice = egData[:,frame:frame+1280].copy().T


    agSlice -= agSlice.min()
    agSlice /= agSlice.max()

    # egSlice -= egSlice.mean()
    egSlice /= (np.abs(egSlice).max() / 0.5)
    egSlice += egOffsets

    sc = p.plot(fullX + frame * rat,
                egSlice,
                lw=0.5,
                c='k',
                alpha=1)
    # myX = fullX + frame * rat
    # for i in range(23):
    #     sc[i].set_data(myX[:,i], egSlice[:,i])

    p.yticks(np.arange(0,23),labels=chNames)
    # fg.get_axes()[0].minorticks_off()


    # X,Y = np.meshgrid(xArC+frame*rat, yArC)
    pcMesh = ax.pcolormesh(X+frame*rat,Y,agSlice, cmap='jet', alpha=0.7)

    p.xlim(0+frame*rat, nnC.sliceLength+frame*rat)


def initFig():
    agSlice = augs[:,:1280].copy()
    egSlice = egData[:,:1280].copy().T

    yAr = np.arange(0, 23)
    xAr = np.linspace(0, nnC.sliceLength, nnC.sliceLengthSamples)

    agSlice -= agSlice.min()
    agSlice /= agSlice.max()

    egSlice -= egSlice.mean()
    egSlice /= (np.abs(egSlice).max() / 0.5)
    egSlice += (np.tile(yAr.reshape((23,1)), (1,nnC.sliceLengthSamples)).T)


    sc = p.plot(np.tile(xAr.reshape(nnC.sliceLengthSamples,1), (1,23)),
                egSlice,
                lw=0.5,
                c='k',
                alpha=1)
    p.yticks(np.arange(0,23),labels=chNames)
    fg.get_axes()[0].minorticks_off()


    X,Y = np.meshgrid(xArC, yArC)
    pcMesh = fg.get_axes()[0].pcolormesh(X,Y,agSlice, cmap='jet', alpha=0.7)

    p.xlim(0, nnC.sliceLength)
    c = p.colorbar(pcMesh)
#%

ani = FuncAnimation(fg,
                    updateFig,
                    frames=list(range(0,augs.shape[1]-1280,8)),
                    interval=1,
                    init_func=initFig)

ani.save('eegVid.mp4', fps=60, extra_args=['-threads', '4'])





#%%
rat = 5/1280
yAr = np.arange(0, 23)
xAr = np.linspace(0, nnC.sliceLength, nnC.sliceLengthSamples)
myX = np.tile(xAr.reshape(nnC.sliceLengthSamples,1), (1,23))

yArC = np.arange(0, 24)-0.5
xArC = np.linspace(0, nnC.sliceLength, nnC.sliceLengthSamples+1)-0.5/nnC.sliceLengthSamples*nnC.sliceLength
X,Y = np.meshgrid(xArC, yArC)
chNames = nnC.loadFileInfo()['ch_names']
egData =(egData.T - egData.mean(1)).T
egOffset = np.tile(yAr.reshape((23,1)), (1,nnC.sliceLengthSamples)).T
tw3 = np.arange(0,23)

def updateFig(frame):

    fr = rat * frame

    fg, ax = p.subplots(tight_layout=True, figsize=(10,6))
    agSlice = augs[:,frame:frame+1280].copy()
    egSlice = egData[:,frame:frame+1280].copy().T


    agSlice -= agSlice.min()
    agSlice /= agSlice.max()


    egSlice /= (np.abs(egSlice).max() / 0.5)
    egSlice += (egOffset)


    sc = p.plot(myX + fr,
                egSlice,
                lw=0.5,
                c='k',
                alpha=1)
    ax.set_yticks(tw3,labels=chNames)
    ax.minorticks_off()


    out = ax.pcolormesh(X+fr,Y,agSlice, cmap='jet', alpha=0.7)

    c = p.colorbar(out)
    ax.set_xlim(0+fr, nnC.sliceLength+fr)
    fg.savefig('image{0:07}.png'.format(frame))
    p.close(fg.number)


from joblib import Parallel, delayed


# Parallel(n_jobs=8, require='sharedmem')(delayed(updateFig)(i) for i in range(0,5528320,8))
