# -*- coding: utf-8 -*-
"""
Created on Thu Aug 26 17:03:45 2021

@author: aa9720

Origninally Allard was looking into clustering EEG in an attempt to classify
EEG as preictal and non-preictal.  This went by the way side and is now only
here as an example if someone would like to dig deeper into this.
"""


import mne
import numpy as np
from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture
from tools import parseCHBpatient
from sklearn.decomposition import PCA
import matplotlib.pyplot as p


patient = 1
length = 5 # seconds

edfs, fileInfo, info = parseCHBpatient(patient=patient)

data = mne.io.read_raw_edf(edfs[2], preload=True)

dataAr = data.get_data()
sampleLength = int(length * data.info['sfreq'])
dataSet = np.array([dataAr[:,a:a+sampleLength].flatten() for a in range(sampleLength, dataAr.shape[1], sampleLength)])

#%% Kmeans
# model = GaussianMixture(n_components=2).fit(dataSet[:300,:])
model = KMeans(n_clusters=4).fit(dataSet)
clCenters = model.cluster_centers_
p.bar(range(dataSet.shape[0]),model.predict(dataSet))


#%% Pca

pca = PCA(n_components=15)
pca.fit(dataSet)

res = pca.transform(dataSet)
p.figure();p.contour((res.T), cmap='jet')
p.colorbar()
