# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 10:05:21 2020

@author: aa9720


A simple script Allar wrote to start thinking about NN foramt.  superceded by
the aiCHBMITclass file.

"""
from tensorflow.keras.layers import Dense, Dropout, LSTM, Conv1D, Flatten, MaxPooling1D, Embedding, SpatialDropout1D
from tensorflow.compat.v1.keras.layers import CuDNNLSTM
from tensorflow.keras.layers import Conv2D, Flatten, MaxPooling2D, Embedding, SpatialDropout2D, Bidirectional, TimeDistributed, Reshape, GRU, RNN
import tensorflow as tf
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint
import os
from parsingCHBMIT import xSlices, ySlices
import platform

if platform.node() == 'Allard-PC':
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

print("Total number of points: {0}\nTotal preictal points: {1}".format(len(ySlices), len(ySlices[ySlices[:,0]==1,0])))

dir_path = os.path.dirname(os.path.realpath(__file__))

model = tf.keras.Sequential()
model.add(Conv2D(32, kernel_size=(2,3), strides=(1,1), input_shape=(23,1280,1), activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
# model.add(Dropout(0.2))
model.add(Conv2D(32, kernel_size=(2,3), strides=(1,1), activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
# model.add(Dropout(0.1))
model.add(Conv2D(32, kernel_size=(2,3), strides=(1,1), activation='relu'))
# model.add(Dropout(0.3))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(32, kernel_size=(2,3), strides=(1,1), activation='relu'))
model.add(Reshape((-1,32)))
model.add(Dropout(0.1))
if not os.uname()[1] == 'automation' and not os.uname()[1] == 'Allard-PC':
    model.add(Bidirectional(CuDNNLSTM(20)))
else:
    model.add(Bidirectional(LSTM(20, dropout=0.5, activation='sigmoid')))
# model.add(Flatten())
# model.add(Dense(20, activation='relu'))

# model.add(Dense(units=128, activation='relu'))
# model.add(Dense(units=64, activation='relu'))
# model.add(Dense(units=32, activation='relu'))
# model.add(Dropout(0.2))
model.add(Dense(units=1, activation='sigmoid'))

model.compile('rmsprop',
              'binary_crossentropy',
              metrics=['accuracy'])


if platform.node() == 'kgcoe-cuda-05':
    filepath=dir_path+"/savedWeights/weights-patient3-{epoch:02d}-{loss:.5f}-ac{accuracy:.5f}.hdf5"
    cp = ModelCheckpoint(filepath, monitor='loss', mode='min', verbose=1, save_best_only=True)
else:
    filepath=dir_path+"/savedWeights/weights-patient3-{epoch:02d}-{loss:.5f}-ac{accuracy:.5f}.hdf5"
    cp = ModelCheckpoint(filepath, monitor='loss', save_best_only=True, mode='min', verbose=1)


model.fit(x=xSlices, y=ySlices[:, 0:1], workers=4, epochs=1000, max_queue_size=8, batch_size=32, callbacks=cp)



#%% Testing



# predictions = model.predict(dGen2)
