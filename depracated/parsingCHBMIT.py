#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 30 14:10:53 2021

@author: allard

Old script from before Allard knew what batch normalization really was.
"""

import os
import mne
from tools import getFilesByEXT, parseCHBpatient
import datetime
import platform
import numpy as np


# Constants for regularization
beta = 0.001
epsilon = 0.002
gamma = 0.02

# Other vars
patient = 3 # inc patient
preIctalWindow = 60*60 # In seconds
interIctalWindow = 60*60
postIctalWindow = 60*60

sliceLength = 5 # in seconds


edfs, fileInfo, info = parseCHBpatient(patient=patient)

#%% Processing
dataList = []
indexList = []
labelList = []
fileIndex = 0
arrayForDL = np.ndarray((23,0))
numFiles = 32
if not platform.node() == 'kgcoe-cuda-05':
    numFiles = 16
for edf in edfs[:numFiles]:
    data = mne.io.read_raw_edf(edf, preload=True)
    # dataList.extend(data.get_data())
    arrayForDL = np.append(arrayForDL, data.get_data(), axis=1)
    labelList.extend(np.zeros((data.get_data().shape[1])))
    indexList.append(data.get_data().shape[1])
    if fileIndex < len(edfs) - 1:
        arrayForDL = np.append(arrayForDL, np.zeros((data.get_data().shape[0], int((fileInfo[fileIndex+1]['sTime'] - fileInfo[fileIndex]['eTime']).seconds * data.info['sfreq']))), axis=1)
        indexList.append(len(np.zeros((int((fileInfo[fileIndex+1]['sTime'] - fileInfo[fileIndex]['eTime']).seconds * data.info['sfreq'])))))
        labelList.extend(np.zeros((int((fileInfo[fileIndex+1]['sTime'] - fileInfo[fileIndex]['eTime']).seconds * data.info['sfreq']))))
    fileIndex += 1

labelList = np.array(labelList)
preIctalWindowSamples = int(preIctalWindow * data.info['sfreq'])
interIctalWindowSamples = int(interIctalWindow * data.info['sfreq'])
postIctalWindowSamples = int(postIctalWindow * data.info['sfreq'])
sliceLengthSamples = int(sliceLength * data.info['sfreq'])

sFreq = data.info['sfreq']

indexList = np.cumsum(indexList)

for f in range(fileIndex):
    if fileInfo[f]['numSeizures'] > 0:
        for seizure in fileInfo[f]['seizureList']:
            startIndex = int(seizure['sTime'] * sFreq)
            endIndex = int(seizure['eTime'] * sFreq)
            labelList[indexList[f*2]+startIndex:indexList[f*2]+endIndex] = 2
            labelList[indexList[f*2]+startIndex - preIctalWindowSamples:indexList[f*2]+startIndex-1] = 1
            labelList[indexList[f*2]+endIndex - preIctalWindowSamples:indexList[f*2]+endIndex+postIctalWindowSamples] = 3


outputLables = np.zeros((3,len(labelList)))

outputLables[0, labelList==1] = 1 # preictal
outputLables[1, labelList==2] = 1 # ictal
outputLables[2, labelList==3] = 1 # postictal
#%% Dump dumby data points

outputLables = outputLables[:, (arrayForDL!=0).max(0)]
labelList = labelList[(arrayForDL!=0).max(0)]

arrayForDL = arrayForDL[:, (arrayForDL!=0).max(0)]

#%% Make xs and ys
xSlices = []
ySlices = []

def regularizeData(dt):
    mn = dt.mean()
    sd = dt.std()
    return gamma * ((dt - mn)/(np.sqrt(sd + epsilon))) + beta

for sliceIndex in range(0, (arrayForDL.shape[1] - arrayForDL.shape[1] % sliceLengthSamples), sliceLengthSamples):
    xSlices.append(regularizeData(arrayForDL[:,sliceIndex:sliceIndex+sliceLengthSamples]))
    ySlices.append(outputLables[:,sliceIndex+sliceLengthSamples - 1])


xSlices = np.array(xSlices).reshape((len(xSlices), 23,-1, 1))
ySlices = np.array(ySlices).reshape((len(xSlices), -1))

# Figure out which individual we want to train on....
# Divide each file into 5 second sections and label each section:
    # preictal
    # ictal
    # interictal
# Train the AI.
#


#%% Slice thigns up
# Cut each file into the right amount of area
# For this task:
    # Preictal = 1h
    # Interictal = ?h




# Before you get to working on this too much do a quick k-means clustering
# The idea is if I can figure out some connection and a better way to
# determine [pre, post, inter, \]ictal.  Rather than abitrary numbers.
