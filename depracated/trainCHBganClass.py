#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 12:12:32 2021

@author: allard
"""
from aiCHBMITclass import nerualNet
import platform
import numpy as np
import matplotlib.pyplot as p

if platform.node() == 'automation' or platform.node() == 'Allard-PC':
    nnC = nerualNet(patient=1)
    nnC.edfStartingFile = 10
    nnC.numEDFsToUse = 7
else:
    nnC = nerualNet(patient=3)
    nnC.edfStartingFile = 30
    nnC.numEDFsToUse = 7 # much longer files in this folder

nnC.loadCHBdata()

nnC.showDataStats()

nn = nnC.buildNN()
nnC.dropExcessData(0.5)

nnC.showDataStats()
nn.predict(nnC.xSlices[1340:1345])
nn.load_weights('bestNets/weightsTF234_noPooling-patient1-kgcoe-cuda-05-ep496-ls0.01297-ac0.53090_threeOutputNN_oneSecSlices.hdf5')
gen = nnC.buildConstuctor()
gan = nnC.buildGAN()
normedX = nnC.xSlices
gan.fit(nnC.xSlices, normedX, batch_size=16, epochs=500)
