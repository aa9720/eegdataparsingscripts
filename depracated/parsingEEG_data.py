# -*- coding: utf-8 -*-
"""
Created on Thu Nov 21 13:10:07 2019

@author: aa9720
"""


import numpy as np
import matplotlib.pyplot as plt
import mne
from mne.io import read_raw_edf
from mne.io import read_raw_fif



pathToEEG_data = r'D:\eegData\chb-mit-scalp-eeg-database-1.0.0\chb01'

filePath = r'D:\eegData\chb-mit-scalp-eeg-database-1.0.0\chb03\chb03_01.edf'

filePath2 = r'D:\eegData\chb-mit-scalp-eeg-database-1.0.0\chb01\chb01_04Marked_raw.fif'
data = read_raw_edf(filePath)

data2 = mne.io.read_raw_fif(filePath2)
