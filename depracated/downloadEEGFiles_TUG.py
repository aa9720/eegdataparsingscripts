# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 06:33:51 2019

@author: aa9720
"""
# %%

import requests
from requests.auth import HTTPBasicAuth
import re
import os


resp = requests.get('https://www.isip.piconepress.com/projects/tuh_eeg/downloads/tuh_eeg_seizure/v1.5.0/edf/train/01_tcp_ar/009/00000906/s001_2003_04_23/00000906_s001_t000.edf', auth=HTTPBasicAuth('UNAME', 'PSK'))

f = open(r'D:\moreEEG\test_raw2.edf', 'wb')
f.write(resp.content)
f.close()

# %% downloading

dumpLocation = 'D:/tuhData/'

baseUrl = 'https://www.isip.piconepress.com/projects/tuh_eeg/downloads/tuh_eeg_seizure/v1.5.1/'


def getSubDirs(url):
    r = requests.get(url, auth=HTTPBasicAuth('nedc', 'nedc_resource'))
    stuff = r.content.decode()
    subdirs = re.findall('<a href=".+">(.+/)</a>', stuff)
    return [r.url + k for k in subdirs]

def getfiles(url):
    r = requests.get(url, auth=HTTPBasicAuth('nedc_tuh_eeg', 'nedc_tuh_eeg'))
    stuff = r.content.decode()
    subdirs = re.findall('<a href=".+">(.+\.\S+)</a>', stuff)
    files = [r.url + k if '.com' not in k else '' for k in subdirs]
    files.remove('')
    return files

def downloadFile(url):
    if '.edf' in url:
        binary = True
    else:
        binary = False
    newPath = url.replace(baseUrl, dumpLocation)
    if os.path.exists(newPath):
        return
    r = requests.get(url, auth=HTTPBasicAuth('nedc_tuh_eeg', 'nedc_tuh_eeg'))
    head, tail = os.path.split(newPath)
    if not os.path.exists(head):
        os.makedirs(head)

    # if binary:
    f = open(newPath, 'wb')
    f.write(r.content)
    # else:
    #     f = open(newPath, 'w')
    #     f.write(r.content.decode())
    f.close()

def getFoldersAndFiles(url):
    dirs = getSubDirs(url)
    files = getfiles(url)
    return dirs, files

def downLoadFolder(url):
    di, fi = getFoldersAndFiles(url)
    for f in fi:
        downloadFile(f)
    for d in di:
        downLoadFolder(d)


downLoadFolder(baseUrl)




# %%
import mne
from mne.io import read_raw_fif
from mne.io import read_raw_edf
import matplotlib.pyplot as plt
data = read_raw_edf(r'D:\moreEEG\test_raw2.edf', preload=True)
import numpy as np

freqs = np.arange(1,125,2)

stuff = mne.time_frequency.tfr_array_morlet(data.get_data().reshape((1,36, 301250)), 250, n_cycles=freqs/2, freqs=freqs, output='avg_power', n_jobs=8)
mne.baseline.rescale(stuff, data.times, (0, 0.1), mode='mean', copy=False)
f, ax=plt.subplots()
mesh = ax.pcolormesh(data.times * 1000, freqs, stuff[0], cmap='RdBu_r', vmax=3e-5, vmin=-3e-5)
ax.set(ylim=freqs[[0, -1]], xlabel='Time (ms)')
f.colorbar(mesh)





# %%

# import pyhht




# dat = data2.get_data()[0,0:25600]
# hht = pyhht.emd.EMD(dat)
# imfs = hht.decompose()
# pyhht.visualization.plot_imfs(dat,imfs)




# start = 756000
# seiDat = data2.get_data()[0,start: start + 25600]
# hhtSei = pyhht.emd.EMD(seiDat)
# imfsSei = hhtSei.decompose()
# pyhht.visualization.plot_imfs(seiDat,imfsSei)
