# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 21:19:37 2020

@author: aa9720
"""
import mne
from mne.io import read_raw_edf
from tools import getFilesByEXT
import pandas as pd
import numpy as np


edfs = getFilesByEXT(r'D:\tuhData\edf\train', '\.edf', True)
tses = getFilesByEXT(r'D:\tuhData\edf\train', '\.tse', True)
txts = getFilesByEXT(r'D:\tuhData\edf\train', '\.txt', True)



#%% Load Data

f = 35

data = read_raw_edf(edfs[f])

info = pd.read_csv(tses[f], sep=' ', header=None, skiprows=[0,1])
info.columns = ['start', 'stop', 'type', 'confidence']

ans = mne.Annotations(info['start'], info['stop']-info['start'], info['type'])

data.set_annotations(ans)

data.plot()
