#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 12:12:32 2021

@author: allard


Old script for basic viewing of CHBMIT trained NNs.
"""
from aiCHBMITclass import nerualNet
import platform
import numpy as np
import matplotlib.pyplot as p
from tools import getFilesByEXT
from nerualNetViewer import display_heatmaps, display_activations, get_activations

patient = 1

if platform.node() == 'automation' or platform.node() == 'Allard-PC':
    nnC = nerualNet(patient=patient)
    nnC.edfStartingFile = 0
    nnC.numEDFsToUse = 7
else:
    nnC = nerualNet(patient=7)
    nnC.edfStartingFile = 9
    nnC.numEDFsToUse = 6 # much longer files in this folder



nnC.loadCHBdata()

nnC.showDataStats()
nn = nnC.buildNN(compatabilityMode=True)

nn.predict(nnC.xSlices[:3])


ws = getFilesByEXT('bestNets/', 'patient{0}.+\.hdf5'.format(patient))
print(ws)
i = int(input('Which one? '))

nn.load_weights(ws[i])
