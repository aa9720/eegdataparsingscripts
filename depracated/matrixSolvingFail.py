# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 10:52:10 2019

@author: aa9720
"""

import mne
from mne.io import read_raw_edf
import numpy as np
import pyhht
import sympy

['FP1-F7',
 'F7-T7',
 'T7-P7',
 'P7-O1',

 'FP1-F3',
 'F3-C3',
 'C3-P3',
 'P3-O1']

rowData = data2.get_data()[:,0]

#                    FP1, F7, T7, P7, O1, F3, C3, P3
mat = sympy.Matrix([[1, -1, 0, 0, 0, 0, 0, 0, rowData[0]],
                    [0, 1, -1, 0, 0, 0, 0, 0, rowData[1]],
                    [0, 0, 1, -1, 0, 0, 0, 0, rowData[2]],
                    [0, 0, 0, 1, -1, 0, 0, 0, rowData[3]],
                    [1, 0, 0, 0, 0, -1, 0, 0, rowData[4]],
                    [0, 0, 0, 0, 0, 1, -1, 0, rowData[5]],
                    [0, 0, 0, 0, 0, 0, 1, -1, rowData[6]],
                    [0, 0, 0, 0, -1, 0, 0, 1, rowData[7]]
                    ])
