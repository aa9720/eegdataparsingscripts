#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 12:12:32 2021

@author: allard
"""
from aiCHBMITclass import nerualNet
import platform

if platform.node() == 'automation' or platform.node() == 'Allard-PC':
    nnC = nerualNet(patient=3)
    nnC.edfStartingFile = 30
    nnC.numEDFsToUse = 7
else:
    print("patient 1")
    nnC = nerualNet(patient=1)
    nnC.edfStartingFile = 0
    nnC.numEDFsToUse = 7 # much longer files in this folder
    nnC.loadCHBdata()
    print("patient 3 now")
    nnC.currentPatient = 3
    nnC.edfStartingFile = 30
    nnC.numEDFsToUse = 7
    nnC.loadCHBdataInfo()
    nnC.loadCHBdata(True)

nn = nnC.buildNN()

# nnC.loadCHBdata()

nnC.showDataStats()

if platform.node() == 'automation' or platform.node() == 'Allard-PC':
    nnC.trainModel(epochs=500, workers=8, batch_size=16)
else:
    nnC.trainModel(epochs=5000, workers=8, batch_size=16)
