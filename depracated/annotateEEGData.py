# -*- coding: utf-8 -*-
"""
Created on Sat Dec  7 20:53:01 2019

@author: aa9720
"""

import numpy as np
import matplotlib.pyplot as plt
import pyedflib
import mne
from mne.io import read_raw_edf
import os
import re

for i in [4]:
    print('{0:02g}'.format(i))
    pathToEEG_data = r'D:\eegData\chb-mit-scalp-eeg-database-1.0.0\chb{0:02g}'.format(i)

    files = os.listdir(pathToEEG_data)


    def parseSummary(summ):
        res = summ.split('\n\n')
        files = []
        for fileNum in res[2:]:
            if len(fileNum) < 10:
                continue
            if fileNum.find('Channel') != -1:
                continue
            seizures = int(re.findall('Seizures in File: (\d+)', fileNum)[0])
            fName = re.findall('File Name: (\S+)', fileNum)[0]
            tempDict = {'NumSeizures': seizures, 'startTimes': [], 'endTimes': [], 'fileName': fName}
            if(seizures > 0):
                tempDict['startTimes'].extend(re.findall('Seizure Start Time: (\d+)', fileNum))
                tempDict['endTimes'].extend(re.findall('Seizure End Time: (\d+)', fileNum))
            for i in range(len(tempDict['startTimes'])):
                tempDict['startTimes'][i] = float(tempDict['startTimes'][i])
                tempDict['endTimes'][i] = float(tempDict['endTimes'][i])
            files.append(tempDict)
        return files


    summaryFile = open(pathToEEG_data+ '\\'+pathToEEG_data.split('\\')[-1]+'-summary.txt')
    summary = summaryFile.read()
    summaryFile.close()

    fileStuff = parseSummary(summary)

    for f in fileStuff:
        if f['NumSeizures'] == 0:
            continue
        data = read_raw_edf(pathToEEG_data + '\\' + f['fileName'])
        anots = mne.Annotations(0, 1, '')
        for s, e in zip(f['startTimes'], f['endTimes']):
            dur = e-s
            anots.append(s, dur, 'Seizure')
        anots.delete(0)
        data.set_annotations(anots)
        data.save(pathToEEG_data + '\\' + f['fileName'].replace('.edf', 'Marked')+'_raw.fif', overwrite=True, proj=True)

