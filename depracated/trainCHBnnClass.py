#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 12:12:32 2021

@author: allard

A really simple script Allard initially used to train the NN.
Now deprecated in favor of the tmsTrain script.
"""
from aiCHBMITclass import nerualNet
import platform
import numpy as np
import matplotlib.pyplot as p

if platform.node() == 'automation' or platform.node() == 'Allard-PC':
    nnC = nerualNet(patient=1)
    nnC.edfStartingFile = 10
    nnC.numEDFsToUse = 7
else:
    nnC = nerualNet(patient=1)
    nnC.edfStartingFile = 10
    nnC.numEDFsToUse = 7 # much longer files in this folder

nnC.loadCHBdata()

nnC.showDataStats()

nn = nnC.buildNN()
nnC.dropExcessData(0.4)

nnC.showDataStats()

if platform.node() == 'automation' or platform.node() == 'Allard-PC':
    ls = nnC.trainModel(epochs=500, workers=8, batch_size=16, fnComments='')
elif platform.node() == 'kgcoe-cuda-04':
    ls = nnC.trainModel(epochs=5000, workers=48, batch_size=16)
elif platform.node() == 'GLE-3241-RA03':
    ls = nnC.trainModel(epochs=50, workers=8, batch_size=16, fnComments='')
else:
    ls = nnC.trainModel(epochs=5000, workers=96, batch_size=16, fnComments='alignedTodesktop')

print(nnC.evalModel(full=True))
