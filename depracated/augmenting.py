#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 10:17:33 2021

@author: allard

Used for basic testing of the augmenting script.  No longer used.
"""

from augment import Augmenter, Superresolution
import matplotlib.pyplot as p
import numpy as np


# %%

numAugs = 500
eegSlice = 2208

ag = Augmenter(numAugs, nn, 'conv2d_3')
augs = ag.getAugs(nnC.xSlices[eegSlice], nn=nn, cnnLayer='conv2d_3')
pred = nn.predict(nnC.xSlices[eegSlice:eegSlice+1])
print(pred[0][0])
sr = Superresolution(ag, 0.01)
sr.runmanyCams(augs, 250)
sr.optimizer.learning_rate.assign(0.001)
sr.runmanyCams(augs, 100)
sr.optimizer.learning_rate.assign(0.0001)
sr.runmanyCams(augs, 50)
# p.figure();p.pcolormesh(sr.fullCam.numpy().squeeze(), cmap='jet');p.colorbar()
# p.figure();p.pcolormesh(nnC.xSlices[eegSlice].squeeze(), cmap='winter');p.colorbar()


#%% Plot the eeg and cmap
out = eps[eegSlice].plot()

xAr = np.linspace(*out.get_axes()[0].get_xlim(), 1280)
yAr = np.arange(0, 23)-0.5
X,Y = np.meshgrid(xAr, yAr)
out.get_axes()[0].pcolormesh(X,Y,sr.fullCam.numpy().squeeze(), cmap='jet', alpha=0.7)

#%% Allard plots eeg

fc = sr.fullCam.numpy().squeeze().copy()

fc -= fc.min()
fc /= fc.max()



yAr = np.arange(0, 23)
xAr = np.linspace(0, nnC.sliceLength, nnC.sliceLengthSamples)

dt = nnC.xSlices[eegSlice:eegSlice+1].squeeze().copy().T
dt -= dt.mean(0)
dt /= (np.abs(dt).max(0)/0.5)
dt += (np.tile(yAr.reshape((23,1)), (1,nnC.sliceLengthSamples)).T)
f = p.figure()
sc = p.plot(np.tile(xAr.reshape(nnC.sliceLengthSamples,1), (1,23)), dt,
               lw=0.5, c='k',
               alpha=1)
f.set_tight_layout(True)

states = ['non-preictal', 'preictal']

p.title('EEG Ep: {0} Pred: {1} Act: {2}'.format(eegSlice, states[preds[eegSlice][0]>0.8], states[nnC.ySlices[eegSlice, 0]==1]), weight='bold')
p.xlabel('Time [s]', weight='bold')

if 'eps' in locals():
    # f.get_axes()[0].set_yticklabels(eps[eegSlice].ch_names)
    p.yticks(np.arange(0,23),labels=eps[eegSlice].ch_names)
    f.get_axes()[0].minorticks_off()
#%
yAr = np.arange(0, 24)-0.5
xAr = np.linspace(0, nnC.sliceLength, nnC.sliceLengthSamples+1)-0.5/nnC.sliceLengthSamples*nnC.sliceLength
X,Y = np.meshgrid(xAr, yAr)
out = f.get_axes()[0].pcolormesh(X,Y,fc, cmap='jet', alpha=0.7)

p.colorbar(out)
p.xlim(0, nnC.sliceLength)
