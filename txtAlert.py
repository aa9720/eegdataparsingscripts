# Download the helper library from https://www.twilio.com/docs/python/install
import os
from twilio.rest import Client

def sendText(msg=''):
    # Find your Account SID and Auth Token at twilio.com/console
    # and set the environment variables. See http://twil.io/secure
    account_sid = 'ACbcfeddfebc2b567125a33983fb47c121'
    with open('key.dpb') as f:
        auth_token = f.read().strip()
    client = Client(account_sid, auth_token)
    if msg == '':
        msg = 'Training Done'
    message = client.messages.create(
        body='{0}'.format(msg),
        from_='+19205415430',
        to='+16037171867')

    print(message.sid)
