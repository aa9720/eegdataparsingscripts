# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 19:19:52 2020

@author: aa9720
"""
import os
import re
import datetime
import platform


def listFiles(path:str, recursive:bool=False):
    """
    A silly simple function to convert from the form of os.walk to a single
    list of files.  Really only useful for getFilesByExt.  (If you are looking
    at this function you should be using os.walk: https://www.tutorialspoint.com/python/os_walk.htm)

    Parameters
    ----------
    path : str
        The directory to list the files from.
    recursive : bool, optional
        Recursive search or not. The default is False.

    Returns
    -------
    list
        a list of all the files in a folder.

    """
    if not recursive:
        return [path + '/' + f for f in os.listdir(path) if os.path.isfile(path + '/' +f)]
    rets = []
    for d, f, fi in os.walk(path):
        if fi != []:
            rets.extend([d + '/' + file for file in fi])
    return rets


def getFilesByEXT(path:str, ext:str, recursive:bool=False):
    """
    A nice simple function to find all the files in a directory with a
    particular name format.  (This function assumes regular expressions)
    If you are not familiar with regular expressions see here:
    https://docs.python.org/3/library/re.html

    Parameters
    ----------
    path : str
        The parent directory you wish to search for files.
    ext : str
        The extension you would like to find (this can be a regular expression
        and does not have to be just an extension).
    recursive : bool, optional
        Whether the search should be recursive or limited to the directory
        specified. The default is False.

    Examples
    ---------
    >>> # finds all files with py in the name
    >>> fs = getFilesByEXT('~/Documents', 'py')

    >>> # finds only the python files (.py files)
    >>> pys = getFilesByEXT('~/Documents', 'py$')

    Returns
    -------
    list
        DESCRIPTION.

    """
    allFiles = listFiles(path, recursive)
    if not '.+' in ext:
        ext = '.+'+ext
    return [file for file in allFiles if len(re.findall(ext, file)) > 0]


def eegPathByMachine():
    """
    A function to get the path to the EEG data on each machine.  If you are
    using a new machine and you did not put the data in ``~/Downloads/eegData/``
    You'll need to edit this function by adding a new if block to this file.

    Returns
    -------
    path : str
        The location of the data.

    """
    path = ''
    if platform.node() == 'GLE-3241-RA03':
        path = os.path.expanduser('D:/eegData/')
    elif platform.node() == 'Allard-PC':
        path = os.path.expanduser('~/Downloads/eegDataLocation/')
    elif platform.node() == 'kgcoe-cuda-05' or platform.node() == 'kgcoe-cuda-04':
        path = os.path.expanduser('~/eegData/eegdata/')
    elif platform.node() == 'deepEEG':
        path = os.path.expanduser('~/Downloads/eegData/')
    elif platform.node() == 'eegNeuralNets':
        path = '/media/allard/data/eegdata/'
    else:
        import warnings
        warnings.warn("You don't have a line for you machine.  Looking for EEG data in the default location.")
        path = os.path.expanduser('~/Downloads/eegData/')
    return path

def parseCHBpatient(patient:int=1, path:str=None):
    """
    A function to parse the CHBMIT txt files for use in the neural network.

    Parameters
    ----------
    patient : int, optional
        The CHB-MIT patient to gather data for. The default is 1.
    path : str, optional
        The path to the data.  You need to specifiy this unelss you have
        modified the `eegPathByMachine` function to know where on your
        machine the data is.
        The default is None.

    Returns
    -------
    edfs : list
        A list of the paths to each edf file from the CHB-MIT.
    fileInfo : list
        A list of each files information in a dictionary.
    info : str
        The raw content of the CHB-MIT txt file.

    """

    if path==None:
        path = eegPathByMachine() + 'chb-mit-scalp-eeg-database-1.0.0/chb{:02}'.format(patient)

    edfs = getFilesByEXT(path, '\d\.edf\Z')
    infoFile = getFilesByEXT(path, 'txt\Z')

    with open(infoFile[0]) as f:
        info = f.read()

    lines = info.split('\n\n')[2:]

    fileInfo = []
    for l in range(len(lines)):
        fileInfo.append({})
        splitLine = lines[l].split('\n')
        fileInfo[l]['fName'] = splitLine[0].split(' ')[-1]
        fileInfo[l]['sTime'] = datetime.datetime.strptime(splitLine[1].split(' ')[-1], '%H:%M:%S')
        et = splitLine[2].split(' ')[-1]
        if int(et.split(':')[0]) > 23:
            et = ':'.join(['00', *et.split(':')[1:]])
        fileInfo[l]['eTime'] = datetime.datetime.strptime(et, '%H:%M:%S')
        fileInfo[l]['numSeizures'] = int(splitLine[3].split(' ')[-1])
        fileInfo[l]['seizureList'] = []
        for i in range(fileInfo[l]['numSeizures']):
            fileInfo[l]['seizureList'].append({})
            fileInfo[l]['seizureList'][i]['sTime'] = int(splitLine[4 + i * 2].split(' ')[-2])
            fileInfo[l]['seizureList'][i]['eTime'] = int(splitLine[5 + i * 2].split(' ')[-2])

    edfs.sort(key=lambda p: int(os.path.basename(p).split('_')[1].split('.')[0]))

    return edfs, fileInfo, info
