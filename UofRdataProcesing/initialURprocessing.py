# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 16:07:39 2022

@author: aa9720
"""

import mne
from tools import getFilesByEXT


pathToFiles = r'D:\tuhData\UR-EEG-Database-RIT'
files = getFilesByEXT(pathToFiles, 'edf')

dtaList = []

for f in files:
    dta = mne.io.read_raw_edf(f)
    anotes = mne.read_annotations(f)
    dtaList.append((dta.info.copy(), dta.annotations.copy(), anotes.copy(), dta.times[-1]))
