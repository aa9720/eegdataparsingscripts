# Neural Net analysis of surface EEG and ECoG to identify pre-ictal activity.

[toc]

## Investigators

* Andrew Allard
  * Principal investigator (RIT)
* Dr. Michael Berg
  * Subject Matter Expert (UR Med)
* Dr Dan Phillips
  * Faculty Advisor (RIT)

## Details

This project is in support of graduate study in Electrical Engineering by Andrew Allard with an anticipated duration extending from August 2021 to May 2022. The goal of this project is to study the properties of an artificial neural network that might be leveraged to improve the detection of pre-ictal seizure activity in individuals diagnosed with epilepsy. The structure of the neural network will consist of four convolution layers with a pooling layer between each convolution layer and a single Long Short-term Memory (LSTM) layer. The properties of the neural network processing will be carried out using a computational technique known as augmented Gradient-Weighted Class Activation Mapping (Grad-CAM). Both scalp EEG and ECoG data will be processed using the neural network and analyzed using augmented Grad-CAM.

### Hypothesis

Neural networks have shown a great ability to recognize pre-ictal activity.  It is hypothesized the neural networks are identifying some particular feature or features associated with the data.  To attempt to identify these features augmented Gradient-weighted Class Activation Mapping (Grad-CAM) will be applied to the patient specific neural networks.

#### Specific Aims

1. Verify the performance of patient specific neural networks at recognizing pre-ictal activity.
2. Determine the features the neural networks are recognizing as pre-ictal activity.
3. Compare the features from different neural networks to see if there are common features across neural networks for patients with similar forms of epilepsy.

### Data to be utilized

#### Scalp EEG

All data provided will be stripped of patient specific information.  The data provided will be raw EEG data with seizures marked in EDF or EDF+ format.  _No patient specific information will be included other than the type of epilepsy involved._

* Scalp EEG records from a total of 10 subjects with focal temporal lobe epilepsy will be incorporated in the study.
  * EEGs from 5 subjects with right temporal lobe epilepsy.
  * EEGs from 5 subjects with left temporal lobe epilepsy.
  * Data files will be labeled in a fashion such that the investigator can distinguish patient data without knowing patient specific information.

Note: _The number of subjects whose data is used may be expanded up to 50 patients worth of anonymized data._

#### RNS EEG from NeuroPace

* RNS (Responsive Neurostimulation) data from 4 to 6 subjects with identified seizure activity will be utilized in the study.
  * Data files will be labeled in a fashion such that the investigator can distinguish patient data without access to patient specific information.

Note: _The number of subjects whose data is used may be expanded up to 15 patients worth of anonymized data._

### Procedure

The EEG data provided will be divided into two sets for each patient. Initially, 80% of the data will be used for training the artificial neural network. The remaining 20%, will be used for assessing the sensitivity and specificity of the neural network performance upon completion of training the neural network. The data will be stored in a secure location and will be password protected for the duration of this project.

A neural network will be trained for each patient using that patient's training dataset (80% of the data for that patient). After the neural network training is completed using a single patient's training data set, the remaining 20% of data will be used to validate the neural networks sensitivity and specificity. The augmented Grad-CAM algorithm will be applied to the neural network information in an attempt to determine what specific features and or sections within the EEG the neural network is using to identify pre-ictal activity.

This process will be repeated on the data from each patient. The features from all of the neural network processing will be compared in an attempt to find any commonality between the features that enable the neural network processing to identify pre-ictal activity.

The findings from this study will be incorporated into a Master of Science thesis dissertation written by the Principle Investigator and may include figures containing plots of scalp EEG and ECoG data utilized in the study.
