On 10/25/2021 Dr Phillips suggested Allard investigate alternate datasets incase Dr Bergs data did not come through.

* [TUH](https://isip.piconepress.com/projects/tuh_eeg/downloads/tuh_eeg_seizure/v1.5.2/)
* [ep-database](http://epilepsy-database.eu/)  Huge but costs ~$3000
* [List of datasets](https://sccn.ucsd.edu/~arno/fam2data/publicly_available_EEG_data.html)
* [Some random collections](https://engineuring.wordpress.com/2009/07/08/downloadable-eeg-data/)
* [interesting mat lab dataset](https://ieee-dataport.org/documents/eeg-dataset-epileptic-seizure-patients)
* [lots of odd datasets](https://openneuro.org/)
* 