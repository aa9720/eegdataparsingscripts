# AI Architecture

This is a short file to describe what the AI will "look" like.

### Basic IO
* Raw EEG signals will be input into the network.
	* EEG fed into the network is in 5 second non overlapping intervals
* The network will have 1 outputs:
	* Preictal (1h before seizure)
### Layer structure
* 2D CNN 32 neurons
* 2D maxpooling
* 2D CNN 32 neurons
* 2D maxpooling
* 2D CNN 32 neurons
* 2D maxpooling
* 2D CNN 32 neurons
* Reshape
* LSTM
* Dense (output layer)