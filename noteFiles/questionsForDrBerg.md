Making a list of questions to ask Dr Berg Monday Sept 13.
* [ ] Is there any particular method for defining preictal and interictal?
	* In terms of time or EEG activity


## Notes from meeting with Dr Berg
### 9/13/2021

#### Project related
* Patient comparison is a good thesis project.
	* Decent but likely to pan out to be (as Allard expects) patient specific.
* It seems very unlikely that the results from the paper (Efficent Epil.... (IEEE paper)) are true.
	* If it were true why has nothing happened to update the existing technology to enhance the seizure prediction device market.
* About 10 years ago there were many competitions and challenges issued with good results.
	* They all seem to have sputtered out without having any substantial affect on the current device technology (which has been on the market for around 15 years)
* Frequency shifts are a good precursor to a seizure.
	* This is not always true but it is a good indication something is changing.
* Many seizures are preceded by artifacts.
	* This may mean the NNs are not going to actually trigger off of anything more than a set of artifacts.
* There is no defined "preictal" time, state or change.
	* Preictal is defined simply as an amount of time leading up to a seizure.
	* 

##### General other facts
* Most epilepsy is treated with medications.
	* This cures ~50% of patients.
	* There are around 25 medications on the market and there is on average a new one every year or two.
		* Though no notable changes to the iEEG devices during this time.
* There are about 3 notable devices on the market that can be implanted and used to predict and ward off seizures.
* _Medications_ includes devices like the RNS (from neuropace)
#### Interesting stuff
* [RNS](https://www.neuropace.com)
	* A really interesting iEEG device used to predict and stop seizures.
	* Problem is it is not as good and struggles to accurately predict seizures in time to ward them off.
	* Based on the data/results these NNs (the ones from Allard thesis) have there is a chance there might be an opportunity to help upgrade the RNS to be a better device.
* iEEG (intercrainial EEG) has no artifacts and is higher amplitude than scalp EEG.

### 9/30/2021
* Got an overview of seizures.
	* tonic-tension
	* clonic-shaking
	* Common seizure types:
		* Spike-n-wave
			* Where there is a short spike followed by a slow wave seizure
		* polyspike-n-wave
			* Lesser slow wave more 
* Will likely have access to intercrainial EEG data 