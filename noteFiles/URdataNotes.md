### 3/7/2022
* Got python scripts clunking away on the UR data at the start of spring break.
* Info about patient break down for processing:


 Patient | Start File | Num Files | Start Aug File | Num Aug Files 
 ------- | ---------- | --------- | -------------- | ------------- 
 1 | 0 | 4 | 2 | 1
 2 | 1 | 1 | 0 | 1
 6 | 0 | 3 | 2 | 1
 8 | 0 | 1 | 0 | 1
 10 | 0 | 1 | 0 | 1




