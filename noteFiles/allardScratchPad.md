This space is where Allard is going to be making mental notes about this project.

Made a notepad of [questionsForDrBerg](noteFiles/questionsForDrBerg.md) so I can keep track of what is what and when things get answered as time goes on.

Made a new note file for the [AI_Architecture](noteFiles/AI_Architecture.md).  That way I have some record of what I am doing.

Narrowing down the thesis:
* The idea at this point is to try making sense of what features the LSTM is using from the CNN and how close they are between patients.
	* If this gets done then it will be time to experiment with trying to train AIs with minimal data to see if we can use all the common features.

### A few observations ATM (9/1/21)
* Focusing in on _just_ preictal improves performance ~3x
	* This makes some sense as the AI is now much more focused rather than broad
* 
### Visualizing
* transfer style learning might be helpful
* 

### Random notes
* 9/3/21 Added a new file for logging finds [allardNoteLog](allardNoteLog.md)
* 9/4/21 Table of patients used in EE paper:
	* ![Pasted image 20210904144205](../assets/Pasted%20image%2020210904144205.png)
* AI back prop equation:
	* $W[n+1]=W[n]+\eta(d[n]-Y[n])X[n]$
		* where W is the weight
		* n is the training step
		* d is the desired output
		* Y is the actual output
		* X is the input
		* $\eta$ is the learning rate
	* Basic steps:
		* Calculate partial derivative of each output WRT to each weight for a given input.
		* This value is multiplied by the learning rate and subtracted from the current weight.
