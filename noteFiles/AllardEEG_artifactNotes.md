# Allard EEG artifact notes

[toc]

## Notes on artifacts

### General rules of thumb

* Any spike observed on 1 and only 1 electrode is an artifact.
  * Brain activity usually is maximum on one probe but is visible on probes surrounding the focal probe with a decaying rate.
* Eye blinks are usually observed on frontal probes with some affect on the next set of probes.
  * 



### Types Artifacts

- Eye blink
- Eye movement
- Physical movements
  - Shifting position or moving hands/feet.
- Ambient noise



## Notes on Seizures

### During Seizure

* Seizures typically last less than 5min.
  * Call 911 if more than 5min.
* Let the person be.
  * Trying to constrain will likely cause more harm than good.
  * Get them away from things that could harm them if they lash out or convulse.

### Random stuff

* Seizures are __over synchronization of brain activity__ causing some portion of the brain to drop into a state of oscillation.
* __Dogs can predict seizures__
  * No one actually knows how.
  * The hunch from those who train these dogs is the dog smells something change.
  * No real active research being done because it cost too much.