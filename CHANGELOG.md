Newest version listed first all changes in the same inverse chronological format as the actual repository versions.





# Version 1
* Resonsible
  * Author: Andrew Allard (aa9720@rit.edu)
  * Advisor: Dan Phillips
  * Project: Allard MSEE thesis (beginning of this repo-spring 2022)
* Setup a new script for the chb-mit neural network
  * This way I can still go back to the old script later.
* Got a new parser to work with the chb-mit data.
* Moved all the chb-mit stuff into a class.
  * This allows better readability and easier debugging
  *  Found on the desktop it is performing better with the added dropout than on the cuda computer.
  *  Added more dropout to the cuda box with hopes of improving it.  Not sure if this will work.
* Reduced the output from 3 to 1 unit to allows for a more focused AI.
* Added a GREAT trained NN set of weights for patient 1 in the CHB-MIT database.
* Updated the NN saving format to insure I will know which machine each set of weights came from.
* _Revamped a bunch of stuff._
  * Got the augmenting up and running.
  * Put in a bunch of plotting stuff.
  * Moved much of the plotting back end over the plotly as it gives better plots.
  * Pulled in twilio for sending txt message alerts.  (You'll need to setup a twilio account and see the [txtAlert.py](https://gitlab.com/aa9720/eegdataparsingscripts/-/blob/master/txtAlert.py))


# Version 0
* Got an initial version of my literature review set and sent out to my committie.
* Added my thesis latex and PDF.
* Got the NN running on the CUDA computer (this required using some different layers which do the same things)

* Reorganized files to clean things up from the original single folder with many files
  * Moved old scripts that are not likely to be used to a separate folder.
  * Moved notes to a subfolder
* Added readme
* Added changelog
* Initial setup.
