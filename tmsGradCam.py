#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  4 11:32:17 2022
@author: allard
"""
# Unicode codes:
# Π = 3a0

# α = 3b1
# β = 3b2
# γ = 3b3
# δ = 3b4
# ε = 3b5
# ζ = 3b6
# η = 3b7
# θ = 3b8
# ι, κ = 3b9, 3ba
# λ = 3bb
# μ = 3bc
#ν, ξ, ο, π, ρ, ς, σ, τ, υ, φ, χ, ψ = [3bd...3c8]
# ω = 3c9

from tmsAugment import findWeightsFile
import pickle
import bz2
import warnings
from alive_progress import alive_bar
import argparse
import re


def gradCam(netToUse='',
            patient=1,
            edfStart=0,
            numEdfsToUse=3,
            ratio=0.5,
            sliceList=[3],
            uofr=False):
    from augment import Augmenter
    from aiCHBMITclass import nerualNet,neuralNetUR
    if not uofr:
        nnC = nerualNet(patient=patient)
    else:
        nnC = neuralNetUR(patient=patient)
    nnC.edfStartingFile = edfStart
    nnC.numEDFsToUse = numEdfsToUse
    nnC.loadData(False)
    nn = nnC.buildNN()
    print(nn.predict(nnC.xSlices[3:4])) # Force the building of the net
    if isinstance(ratio, float):
        nnC.dropExcessData(ratio)
    if isinstance(sliceList, str):
        if sliceList.lower() == 'all':
            sliceList = range(nnC.xSlices.shape[0])
        else:
            sliceList = [int(a) for a in sliceList.split(',') if a != '[' and a != ']']
    elif isinstance(sliceList, int):
        sliceList = [sliceList]
    if not netToUse == '' and not netToUse is None:
        nn.load_weights(findWeightsFile(netToUse))
    else:
        raise Exception('Need NN weights file to perform cams on')
    layerNames = [l.name for l in nn.layers if 'conv' in l.name][::2]
    augList = [[] for i in range(len(layerNames))]
    augers = [Augmenter(1, nn, layer) for layer in layerNames]
    with alive_bar(len(sliceList)) as bar:
        for sliceIndex in range(len(sliceList)):
            for i in range(len(layerNames)):
                tp = augers[i].make_gradcam_heatmap(
                    nnC.xSlices[sliceList[sliceIndex]:sliceList[sliceIndex]+1],
                    nn)
                augList[i].append(tp.numpy())
            bar()
    return augList






if __name__ == '__main__':
    psr = argparse.ArgumentParser(description='This script uses a pretrained network and the augmenting script to run many augmented version of the data through the network in an attempt to figure out what the network learned.')

    psr.add_argument('patient', type=int, help='The patient to train the AI to predict seizures for')
    psr.add_argument('--uofr', type=bool, help='whether or not to use the U of R data for training.', required=False, default=False)
    psr.add_argument('--edf_start', help='EDF file to start with', default=0, type=int, required=False)
    psr.add_argument('--num_edfs', help='Number of EDF files to use', default=7, type=int, required=False)



    psr.add_argument('--net', help='The weights file for the neural network to use.', required=False, type=str, default=None)
    psr.add_argument('--of', help='Comments to put on the end of the file name', default='', type=str, required=False)
    psr.add_argument('--ratio', help='Ratio of preictal to non preictial', default=0.5, required=False)
    psr.add_argument('--slice', help='The index of the EEG slice to augment or a list of them.  \'all\' will augment all the slices.', required=False, default=0)
    psr.add_argument('--txt', required=False, default=False, action='store_true', dest='txtAlert', help='If added a text alert will be sent after training is done.')

    args = psr.parse_args()
    ret = gradCam(netToUse=args.net,
                  ratio=args.ratio,
                  edfStart=args.edf_start,
                  numEdfsToUse=args.num_edfs,
                  patient=args.patient,
                  sliceList=args.slice,
                  uofr=args.uofr)
    import os
    # path = os.getcwd()
    file = os.getcwd() + os.sep + args.of + '.pbz2'
    with bz2.BZ2File(file, 'wb') as f:
        pickle.dump(ret, f)
